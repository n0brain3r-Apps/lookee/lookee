//
//  LDocument+PDF.m
//  Writeability
//
//  Created by Ryan Blonna on 26/10/14.
//  Copyright (c) 2014 CNL. All rights reserved.
//


#import "LDocument+PDF.h"

#import "LConstants.h"

#import "LPage.h"

#import <Rendering/LGLCanvas.h>

#import <Utilities/LPDF.h>

#import <OpenGLES/ES2/glext.h>




//*********************************************************************************************************************//
#pragma mark -
#pragma mark LDocument+PDF
#pragma mark -
//*********************************************************************************************************************//




@implementation LDocument (PDF)

#pragma mark - Protected Methods
//*********************************************************************************************************************//

- (NSUInteger)getPDFPageCount
{
    NSUInteger pageCount = 0;

    LPDFInfo([self URL], &pageCount, NULL);

    return pageCount;
}

- (void)exportPDFToDestination:(NSURL *)destination
{
    if ([self.pages count]) {
        [LGL queueAsyncOperation:^{
            LGLCanvas   *canvas     = [[LGLCanvas alloc]
                                       initWithWidth  :LC__MaximumDocumentDimension
                                       height         :LC__MaximumDocumentDimension
                                       scale          :[self scale]];

            [canvas setBackgroundColor:[UIColor whiteColor]];

            UIGraphicsBeginPDFContextToFile([destination path], CGRectZero, nil);

            NSUInteger  location    = 1;

            for (LPage *page in [self pages]) {
                CGRect bounds = ((CGRect){
                    .origin = {0},
                    .size   = [page dimensions]
                });

                [canvas clear];
                [canvas queueRenderOperation:^{[page renderWithScale:self.scale];}];

                glFinish();

                UIGraphicsBeginPDFPageWithInfo(bounds, nil);

                [[canvas imageWithScale:1.] drawInRect:((CGRect) {
                    {0}, {LC__MaximumDocumentDimension, LC__MaximumDocumentDimension}
                })];

                [self.delegate document:self didExportPageAtLocation:location ++];
            }
            
            UIGraphicsEndPDFContext();
        }];
    }
}

@end
