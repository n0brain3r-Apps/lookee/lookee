//
//  LDocument+Annotator.m
//  Writeability
//
//  Created by Ryan on 7/4/14.
//  Copyright (c) 2014 CNL. All rights reserved.
//


#import "LDocument+Annotator.h"

#import "LPage.h"




//*********************************************************************************************************************//
#pragma mark -
#pragma mark LPage+UndoManager
#pragma mark -
//*********************************************************************************************************************//




@implementation LDocument (Annotator)

#pragma mark - Properties
//*********************************************************************************************************************//

@dynamic annotatorIndex;

- (NSUInteger)annotatorIndex
{
    if (![self.pages count]) {
        return NSNotFound;
    }

    return [self.pages[ 0 ] annotatorIndex];
}

- (void)setAnnotatorIndex:(NSUInteger)annotatorIndex
{
    for (LPage *page in [self pages]) {
        [page setAnnotatorIndex:annotatorIndex];
    }
}

- (NSUInteger)annotatorCount
{
    if (![self.pages count]) {
        return 0;
    }

    return [self.pages[ 0 ] annotatorCount];
}


#pragma mark - Public Methods
//*********************************************************************************************************************//

- (void)addAnnotators:(NSUInteger)count
{
    for (LPage *page in [self pages]) {
        [page addAnnotators:count];
    }
}

- (void)addAnnotators:(NSUInteger)count hidden:(BOOL)hidden
{
    for (LPage *page in [self pages]) {
        [page addAnnotators:count hidden:hidden];
    }
}

- (void)addAnnotators:(NSUInteger)count atIndex:(NSUInteger)index
{
    for (LPage *page in [self pages]) {
        [page addAnnotators:count atIndex:index];
    }
}

- (void)addAnnotators:(NSUInteger)count atIndex:(NSUInteger)index hidden:(BOOL)hidden
{
    for (LPage *page in [self pages]) {
        [page addAnnotators:count atIndex:index hidden:hidden];
    }
}

- (void)removeAllAnnotators
{
    for (LPage *page in [self pages]) {
        [page removeAllAnnotators];
    }
}

- (void)removeAnnotatorAtIndex:(NSUInteger)index
{
    for (LPage *page in [self pages]) {
        [page removeAnnotatorAtIndex:index];
    }
}

- (void)hideAnnotatorAtIndex:(NSUInteger)index
{
    for (LPage *page in [self pages]) {
        [page hideAnnotatorAtIndex:index];
    }
}

- (void)showAnnotatorAtIndex:(NSUInteger)index
{
    for (LPage *page in [self pages]) {
        [page showAnnotatorAtIndex:index];
    }

}

- (void)hideAllAnnotators
{
    for (LPage *page in [self pages]) {
        [page hideAllAnnotators];
    }
}

- (void)showAllAnnotators
{
    for (LPage *page in [self pages]) {
        [page showAllAnnotators];
    }
}

@end
