//
//  LDocument+Annotator.h
//  Writeability
//
//  Created by Ryan on 7/4/14.
//  Copyright (c) 2014 CNL. All rights reserved.
//


#import "LDocument.h"


@class LAnnotator;


@interface LDocument (Annotator)

@property (nonatomic, readwrite	, assign) NSUInteger annotatorIndex;
@property (nonatomic, readonly	, assign) NSUInteger annotatorCount;


- (void)addAnnotators:(NSUInteger)count;
- (void)addAnnotators:(NSUInteger)count hidden:(BOOL)hidden;
- (void)addAnnotators:(NSUInteger)count atIndex:(NSUInteger)index;
- (void)addAnnotators:(NSUInteger)count atIndex:(NSUInteger)index hidden:(BOOL)hidden;

/**
 * The following methods will only
 * hide the annotator at index 0.
 */
- (void)removeAllAnnotators;
- (void)removeAnnotatorAtIndex:(NSUInteger)index;

- (void)hideAnnotatorAtIndex:(NSUInteger)index;
- (void)showAnnotatorAtIndex:(NSUInteger)index;

- (void)hideAllAnnotators;
- (void)showAllAnnotators;

@end
