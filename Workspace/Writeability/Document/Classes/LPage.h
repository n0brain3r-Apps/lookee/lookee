//
//  LDocumentPage.h
//  Writeability
//
//  Created by Ryan on 2/5/14.
//  Copyright (c) 2014 CNL. All rights reserved.
//


#import <UIKit/UIKit.h>


@class      LDocument;

@class      LAnnotator;

@protocol   LPageDelegate;



@interface LPage : NSObject

@property (nonatomic, readonly  , weak  ) LDocument     *document;

@property (nonatomic, readonly  , assign) CGSize        dimensions;
@property (nonatomic, readonly  , assign) CGFloat       scale;

@property (nonatomic, readonly  , assign) NSUInteger    pageNumber;

@property (nonatomic, readwrite	, assign) NSUInteger    annotatorIndex;
@property (nonatomic, readonly	, assign) NSUInteger    annotatorCount;

@property (nonatomic, readonly  , strong) LAnnotator    *currentAnnotator;
@property (nonatomic, readonly  , strong) LAnnotator    *lastAnnotator;


+ (instancetype)pageForDocument:(LDocument *)document pageNumber:(NSUInteger)pageNumber;


- (instancetype)initWithDocument:(LDocument *)document pageNumber:(NSUInteger)pageNumber;

- (void)addDelegate:(id<LPageDelegate>)delegate;
- (void)removeDelegate:(id<LPageDelegate>)delegate;

- (id)tagForAnnotatorAtIndex:(NSUInteger)index;
- (void)setTag:(id)tag forAnnotatorAtIndex:(NSUInteger)index;

- (BOOL)isCachedAtScale:(CGFloat)scale;
- (void)cacheWithScale:(CGFloat)scale;
- (void)renderWithScale:(CGFloat)scale;

- (void)addAnnotators:(NSUInteger)count;
- (void)addAnnotators:(NSUInteger)count hidden:(BOOL)hidden;
- (void)addAnnotators:(NSUInteger)count atIndex:(NSUInteger)index;
- (void)addAnnotators:(NSUInteger)count atIndex:(NSUInteger)index hidden:(BOOL)hidden;


/** 
 * The following methods can only 
 * hide the annotator at index 0.
 */
- (void)removeAllAnnotators;
- (void)removeAnnotatorAtIndex:(NSUInteger)index;

- (void)hideAnnotatorAtIndex:(NSUInteger)index;
- (void)showAnnotatorAtIndex:(NSUInteger)index;

- (void)hideAllAnnotators;
- (void)showAllAnnotators;

@end


@protocol LPageDelegate <NSObject>
@required

- (void)pageDidUpdate:(LPage *)page;


@optional

- (void)
pageDidMoveTool :(LPage *   )page
withColor       :(UIColor * )color
toLocation      :(CGPoint   )location
atAnnotatorIndex:(NSUInteger)index;

@end
