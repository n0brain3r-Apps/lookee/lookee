//
//  LDocumentPage.m
//  Writeability
//
//  Created by Ryan on 2/5/14.
//  Copyright (c) 2014 CNL. All rights reserved.
//


#import "LPage.h"

#import "LConstants.h"

#import "LDocument.h"

#import "SDImageCache.h"

#import <Annotation/LAnnotator.h>

#import <Rendering/LGLQuad.h>
#import <Rendering/LGLTexture.h>

#import <Utilities/LGroupProxy.h>
#import <Utilities/LGeometry.h>

#import <Utilities/NSObject+Utilities.h>




//*********************************************************************************************************************//
#pragma mark -
#pragma mark LDocumentPage
#pragma mark -
//*********************************************************************************************************************//




@interface LPage () <LADelegate>

@property (nonatomic, readonly, strong) SDImageCache    *imageCache;

@property (nonatomic, readonly, strong) NSMutableArray  *annotators;

@property (nonatomic, readonly, strong) LGroupProxy     <LPageDelegate>*delegates;

@end

@implementation LPage

#pragma mark - Class Methods
//*********************************************************************************************************************//

+ (NSMapTable *)instances
{
    static NSMapTable       *instances  = nil;
    static dispatch_once_t  predicate   = 0;

    dispatch_once(&predicate, ^{
        instances = [NSMapTable strongToWeakObjectsMapTable];
    });

    return instances;
}

+ (instancetype)pageForDocument:(LDocument *)document pageNumber:(NSUInteger)pageNumber
{
    return [[self alloc] initWithDocument:document pageNumber:pageNumber];
}


#pragma mark - Constructors/Destructors
//*********************************************************************************************************************//

- (instancetype)init __ILLEGALMETHOD__;

- (instancetype)initWithDocument:(LDocument *)document pageNumber:(NSUInteger)pageNumber
{
    id instances = [LPage instances];

    id namespace = [self generateNameSpaceForDocument:document atPageNumber:pageNumber];

    id page;

    @synchronized(instances) {
        page = [instances objectForKey:namespace];
    }

    if (page) {
        return page;
    }

    self = [super init];

    if (self) {
        _document   = document;

        _pageNumber = pageNumber;

        _dimensions = [self getDimensions];

        _imageCache = [[SDImageCache alloc] initWithNamespace:namespace];

        _annotators = [NSMutableArray new];

        _delegates  = [LGroupProxy proxy];

        [self initialize];
    }

    return self;
}

- (void)initialize
{
    [self.imageCache clearMemory];
    [self.imageCache clearDiskOnCompletion:nil];

    $weakify(self) {
        [self observerForFileAtPath:[self.document URL].path] (^{
            $strongify(self) {
                [self.imageCache clearMemory];
                [self.imageCache clearDiskOnCompletion:nil];
            }
        });
    }

    [self addAnnotators:1];
}

- (void)dealloc
{
    DBGMessage(@"deallocated %@", self);

    [self.imageCache clearMemory];
    [self.imageCache clearDiskOnCompletion:nil];
}


#pragma mark - Properties
//*********************************************************************************************************************//

@dynamic scale;

- (CGFloat)scale
{
    return [self.document scale];
}


@dynamic currentAnnotator;

- (LAnnotator *)currentAnnotator
{
    return [self annotatorAtIndex:self.annotatorIndex];
}


@dynamic lastAnnotator;

- (LAnnotator *)lastAnnotator
{
    if ([self annotatorCount]) {
        return [self annotatorAtIndex:[self annotatorCount]-1];
    }

    return nil;
}

@dynamic annotatorCount;

- (NSUInteger)annotatorCount
{
    return [self.annotators count];
}


#pragma mark - Private Methods
//*********************************************************************************************************************//

- (NSString *)generateNameSpaceForDocument:(LDocument *)document atPageNumber:(NSUInteger)pageNumber
{
    return @Format(@"%@:%lu", document.URL.path, pageNumber);
}

- (NSString *)generateContentKeyForScale:(CGFloat)scale
{
    return @Format(@"%.2fx%.2f@%.2fx", self.dimensions.width, self.dimensions.height, scale);
}

- (CGSize)getDimensions
{
    NSString *fileExtension = [self.document.URL.pathExtension uppercaseString];

    NSString *selname  = @Format(@"get%@Dimensions", fileExtension);

    SEL selector = NSSelectorFromString(selname);

    if ([self respondsToSelector:selector]) {
        NSInvocation    *invocation = [self invocationForSelector:selector];

        CGSize          dimensions  ; [invocation invoke], [invocation getReturnValue:&dimensions];

        CGSizeClampToScalar(dimensions, LC__MaximumDocumentDimension);

        return dimensions;
    }

    return CGSizeZero;
}

- (void)renderContent
{
    NSString *fileExtension = [self.document.URL.pathExtension uppercaseString];

    NSString *selname  = @Format(@"render%@Content", fileExtension);

    SEL selector = NSSelectorFromString(selname);

    if ([self respondsToSelector:selector]) {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
        [self performSelector:selector];
#pragma clang diagnostic pop
    }
}


#pragma mark - Public Methods
//*********************************************************************************************************************//

- (void)addDelegate:(id<LPageDelegate>)delegate
{
    [self.delegates $addObject:delegate];
}

- (void)removeDelegate:(id<LPageDelegate>)delegate
{
    [self.delegates $removeObject:delegate];
}

- (id)tagForAnnotatorAtIndex:(NSUInteger)index
{
    return [self annotatorAtIndex:index].tag;
}

- (void)setTag:(id)tag forAnnotatorAtIndex:(NSUInteger)index
{
    [self annotatorAtIndex:index].tag = (tag);
}

- (BOOL)isCachedAtScale:(CGFloat)scale
{
    return !![self.imageCache imageFromDiskCacheForKey:[self generateContentKeyForScale:scale]];
}

- (void)cacheWithScale:(CGFloat)scale
{
    if (!IsZero(scale)) {
        NSString    *contentKey     = [self generateContentKeyForScale:scale];

        UIImage     *contentImage   = [self.imageCache imageFromDiskCacheForKey:contentKey];

        if (!contentImage) {
            CGSize size = {
                LC__MaximumDocumentDimension,
                LC__MaximumDocumentDimension
            };

            UIGraphicsBeginImageContextWithOptions(size, YES, scale);
            CGContextRef context = UIGraphicsGetCurrentContext();

            [self renderContent];
            [self cacheCurrentImage];
            
            UIGraphicsEndImageContext();
        }
    }
}

- (void)renderWithScale:(CGFloat)scale
{
    if (!IsZero(scale)) {
        CGFloat scale_= [LGL scale];

        [LGL setScale:scale];
        {
            NSString    *key        = @Format(@"content-renderer@%.2fx", scale);

            LGLQuad     *renderer   = [LGL objectPool][key];

            if (!renderer) {
                LGLTexture *texture
                =
                [[LGLTexture alloc]
                 initWithWidth     :LC__MaximumDocumentDimension
                 height            :LC__MaximumDocumentDimension
                 scale             :scale];

                renderer = [LGL objectPool][key]
                =
                [[LGLQuad alloc] initWithTexture:texture];
            }

            renderer.target =
            renderer.region = ((CGRect) {
                .origin = (CGPointZero),
                .size   = [self dimensions]
            });

            if (renderer.context != Voidify(self)) {
                renderer.context  = Voidify(self);

                NSString    *contentKey     = [self generateContentKeyForScale:scale];

                UIImage     *contentImage   = [self.imageCache imageFromDiskCacheForKey:contentKey];

                [renderer.texture drawBitmap:^{
                    if (contentImage) {
                        [contentImage drawAtPoint:CGPointZero];
                        return;
                    }

                    [self renderContent];
                    [self cacheCurrentImage];
                }];
            }
            
            [renderer render];
            
            for (LAnnotator *annotator in [self annotators]) {
                [annotator render];
            }
        }
        [LGL setScale:scale_];
    }
}

- (void)addAnnotators:(NSUInteger)count
{
	[self addAnnotators:count hidden:NO];
}

- (void)addAnnotators:(NSUInteger)count hidden:(BOOL)hidden
{
    [self addAnnotators:count atIndex:self.annotatorCount hidden:NO];
}

- (void)addAnnotators:(NSUInteger)count atIndex:(NSUInteger)index
{
    [self addAnnotators:count atIndex:index hidden:NO];
}

- (void)addAnnotators:(NSUInteger)count atIndex:(NSUInteger)index hidden:(BOOL)hidden
{
    $weakify(self) {
        for (NSUInteger counter = 0; counter < count; ++ counter, ++ index) {
            LAnnotator *annotator = [LAnnotator annotatorWithDelegate:self];
            {
                [annotator setHidden:hidden];
            }

            [self.annotators insertObject:annotator atIndex:index];

            [annotator observerForKeyPath:@Keypath(annotator.annotationTool)](^(LKVONotification *notification) {
                $strongify(self) {
                    LATool *annotationTool;
                    {
                        annotationTool = [notification oldValue];

                        [annotationTool destroyObserversForKeyPath:@Keypath(annotationTool.currentLocation)];

                        annotationTool = [notification newValue];
                    }

                    $weakify(self, annotationTool) {
                        [annotationTool observerForKeyPath:@Keypath(annotationTool.currentLocation)] (^{
                            $strongify(self, annotationTool) {
                                if ([self.delegates respondsToSelector:@selector(pageDidMoveTool:
                                                                                 withColor      :
                                                                                 toLocation     :
                                                                                 atAnnotatorIndex:)])
                                {
                                    [[self delegates]
                                     pageDidMoveTool    :self
                                     withColor          :annotationTool.properties.color
                                     toLocation         :annotationTool.currentLocation
                                     atAnnotatorIndex   :index];
                                }
                            }
                        });
                    }
                }
            });
        }
    }
}

- (LAnnotator *)annotatorAtIndex:(NSUInteger)index
{
	return [self annotators][ index ];
}

- (void)removeAllAnnotators
{
    LAnnotator *annotator = [self annotators][ 0 ];
    [self.annotators removeObjectAtIndex:0];

    for (LAnnotator *annotator in [self annotators]) {
        [annotator destroyObserversForKeyPath:@Keypath(annotator.annotationTool)];
    }

	[self.annotators removeAllObjects];

	[annotator setHidden:YES];
	[annotator removeAllAnnotations];

    [self.annotators addObject:annotator];

	[self setAnnotatorIndex:0];
}

- (void)removeAnnotatorAtIndex:(NSUInteger)index
{
	if (index > 0) {
        LAnnotator *annotator = self.annotators[index];
        [annotator destroyObserversForKeyPath:@Keypath(annotator.annotationTool)];

		[self.annotators removeObjectAtIndex:index];

		if ([self annotatorIndex] == index) {
			[self setAnnotatorIndex:0];
		}
	} else {
		[self.annotators[ 0 ] setHidden:YES];
		[self.annotators[ 0 ] removeAllAnnotations];
	}
}

- (void)hideAnnotatorAtIndex:(NSUInteger)index
{
    [self.annotators[index] setHidden:YES];
}

- (void)showAnnotatorAtIndex:(NSUInteger)index
{
    [self.annotators[index] setHidden:NO];
}

- (void)hideAllAnnotators
{
    for (LAnnotator *annotator in [self annotators]) {
        [annotator setHidden:YES];
    }
}

- (void)showAllAnnotators
{
    for (LAnnotator *annotator in [self annotators]) {
        [annotator setHidden:NO];
    }
}


#pragma mark - Private Methods
//*********************************************************************************************************************//

- (void)cacheCurrentImage
{
    CGContextRef    context = UIGraphicsGetCurrentContext();

    CGImageRef      image   = CGBitmapContextCreateImage(context);

    if (image) {
        CGAffineTransform transform = CGContextGetCTM(context);

        [[self imageCache]
         storeImage :
         [UIImage
          imageWithCGImage  :image
          scale             :transform.a
          orientation       :UIImageOrientationForTransform(transform)]
         forKey     :[self generateContentKeyForScale:transform.a]];

        CGImageRelease(image);
    }
}


#pragma mark - LADelegate
//*********************************************************************************************************************//

- (void)annotatorDidUpdate:(LAnnotator *)annotator
{
    if ([self.delegates respondsToSelector:@selector(pageDidUpdate:)]) {
        [self.delegates pageDidUpdate:self];
    }
}

@end
