//
//  LThicknessPalette.m
//  Writeability
//
//  Created by Jelo Agnasin on 11/25/13.
//  Copyright (c) 2013 CNL. All rights reserved.
//

#import "LThicknessPalette.h"

#import "LConstants.h"

//*********************************************************************************************************************//
#pragma mark -
#pragma mark LThicknessPalette
#pragma mark -
//*********************************************************************************************************************//

@interface LThicknessPalette ()

@property (nonatomic, readwrite, strong) UIImageView *blackSlider;
@property (nonatomic, readwrite, strong) UIImageView *graySlider;

@property (nonatomic, readwrite, strong) CALayer *blackMask;
@property (nonatomic, readwrite, strong) CALayer *grayMask;

@property (nonatomic, readwrite, strong) CAShapeLayer *notch;
@property (nonatomic, readwrite, strong) NSArray *markers;

@end

@implementation LThicknessPalette


#pragma mark - Static Constants
//*********************************************************************************************************************//

static const CGFloat kRefFrameInset = 6.0;

static const CGFloat kNotchTop = 4.0;
static const CGFloat kNotchWidth = 6.0;
static const CGFloat kNotchHeight = 26.0;
static const CGFloat kNotchCornerRadius = 2.0;

static const CGFloat kMarkerWidth = 2.0;
static const CGFloat kMarkerHeight = 4.0;

static NSString *const kWidthSliderBlackImage = @"width-slider-black";
static NSString *const kWidthSliderGrayImage = @"width-slider-gray";


#pragma mark - Memory Management
//*********************************************************************************************************************//

- (instancetype)init
{
    self = [super init];
    if (!self) return nil;
    
    self.blackSlider = [[UIImageView alloc] initWithImage:[UIImage imageNamed:kWidthSliderBlackImage]];
    self.graySlider = [[UIImageView alloc] initWithImage:[UIImage imageNamed:kWidthSliderGrayImage]];
    
    [self addSubview:self.graySlider];
    [self addSubview:self.blackSlider];
    
    return self;
}


#pragma mark - Layout Subviews
//*********************************************************************************************************************//

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    CGRect frame;
    
    frame = self.blackSlider.frame;
    frame.origin.x = (self.bounds.size.width - frame.size.width) / 2.0;
    frame.origin.y = (self.bounds.size.height - frame.size.height) / 2.0;
    self.blackSlider.frame = frame;
    self.graySlider.frame = frame;
    
    [self setupMarkers];
}


#pragma mark - Setup Methods
//*********************************************************************************************************************//

- (void)setupNotch
{
    if (!self.notch) {
        CGRect frame = CGRectMake(0, 0, kNotchWidth, kNotchHeight);
        
        self.notch = [[CAShapeLayer alloc] init];
        self.notch.fillColor = [UIColor LTealColor].CGColor;
        self.notch.path = [UIBezierPath bezierPathWithRoundedRect:frame cornerRadius:kNotchCornerRadius].CGPath;
        
        frame = self.notch.frame;
        frame.origin.y = kNotchTop;
        frame.size.width = kNotchWidth;
        frame.size.height = kNotchHeight;
        self.notch.frame = frame;
        
        [self.layer addSublayer:self.notch];
    }
    
    CGRect frame = self.notch.frame;
    CGRect refFrame = CGRectInset(self.blackSlider.frame, kRefFrameInset, 0);
    
    CGFloat offset = frame.size.width / 2.0;
    
    frame.origin.x = refFrame.origin.x + self.index * (refFrame.size.width / 3.0) - offset;
    self.notch.frame = frame;
    [self setupSliders];
}

- (void)setupMarkers
{
    if (!self.markers) {
        NSMutableArray *markers = [NSMutableArray arrayWithCapacity:4];
        for (NSInteger x = 0; x < 4; x++) {
            CAShapeLayer *marker = [[CAShapeLayer alloc] init];
            marker.fillColor = [UIColor LBorderColor].CGColor;
            marker.path = [UIBezierPath bezierPathWithRect:CGRectMake(0, 0, kMarkerWidth, kMarkerHeight)].CGPath;
            marker.frame = CGRectMake(0, 0, kMarkerWidth, kMarkerHeight);
            [self.layer insertSublayer:marker atIndex:0];
            
            [markers addObject:marker];
        }
        
        self.markers = markers;
    }
    
    CGRect refFrame = CGRectInset(self.blackSlider.frame, kRefFrameInset, 0);
    refFrame.origin.y = kNotchTop + kNotchHeight - kMarkerHeight;
    
    [self.markers enumerateObjectsUsingBlock:^(CAShapeLayer *marker, NSUInteger idx, BOOL *stop) {
        CGRect frame = refFrame;
        frame.origin.x += idx * (refFrame.size.width / 3.0);
        marker.frame = frame;
    }];
}

// !!!: should setup sliders right after setting up notch's frame
- (void)setupSliders
{
    if (!self.blackMask) {
        self.blackMask = [[CALayer alloc] init];
        self.blackMask.backgroundColor = [UIColor whiteColor].CGColor;
        self.blackSlider.layer.mask = self.blackMask;
    }
    if (!self.grayMask) {
        self.grayMask = [[CALayer alloc] init];
        self.grayMask.backgroundColor = [UIColor whiteColor].CGColor;
        self.graySlider.layer.mask = self.grayMask;
    }
    
    
    CGRect frame;
    CGRect notchFrame = self.notch.frame;
    
    frame = self.blackSlider.bounds;
    frame.size.width = notchFrame.origin.x;
    self.blackMask.frame = frame;
    
    
    frame = self.graySlider.bounds;
    frame.origin.x = notchFrame.origin.x;
    frame.size.width = (frame.size.width - frame.origin.x);
    self.grayMask.frame = frame;
}


#pragma mark - Properties
//*********************************************************************************************************************//

- (void)setIndex:(NSInteger)index
{
    _index = index;
    [self setupNotch];
}


#pragma mark - Touches
//*********************************************************************************************************************//

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self handleTouch:touches.anyObject];
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self handleTouchEnded:touches.anyObject];
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self handleTouchEnded:touches.anyObject];
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self handleTouch:touches.anyObject];
}

- (void)handleTouch:(UITouch *)touch
{
    CGPoint location = [touch locationInView:self];
    
    CGRect frame = self.notch.frame;
    CGRect refFrame = CGRectInset(self.blackSlider.frame, kRefFrameInset, 0);
    
    CGFloat offset = frame.size.width / 2.0;
    
    CGFloat minX = refFrame.origin.x - offset;
    CGFloat maxX = refFrame.origin.x + refFrame.size.width - offset;
    
    frame.origin.x = location.x < minX ? minX : location.x > maxX ? maxX : location.x;
    self.notch.frame = frame; // automatic animation with CALayers
    [self setupSliders];
}

- (void)handleTouchEnded:(UITouch *)touch
{
    CGPoint location = [touch locationInView:self];
    
    CGRect refFrame = CGRectInset(self.blackSlider.frame, kRefFrameInset, 0);
    
    NSInteger index = (location.x - refFrame.origin.x) / (refFrame.size.width / 3.0);
    
    self.index = index < 0 ? 0 : index > 3 ? 3 : index;
    [self.delegate thicknessPalette:self didChangeIndex:self.index];
}

@end

