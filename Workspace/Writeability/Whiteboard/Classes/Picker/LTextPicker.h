//
//  LTextPicker.h
//  Writeability
//
//  Created by Jelo Agnasin on 11/25/13.
//  Copyright (c) 2013 CNL. All rights reserved.
//

#import <UIKit/UIKit.h>

@class LTextPicker;

@protocol LTextPickerDelegate <NSObject>

- (void)textPicker:(LTextPicker *)textPicker didChangeColor:(UIColor *)color;
- (void)textPicker:(LTextPicker *)textPicker didChangeSize:(NSNumber *)size;
- (void)textPicker:(LTextPicker *)textPicker didChangeFontName:(NSString *)fontName;

@end


@interface LTextPicker : UIView

@property (nonatomic, readwrite, weak) id <LTextPickerDelegate> delegate;

@property (nonatomic, readwrite, strong) UIColor *color;
@property (nonatomic, readwrite, strong) NSNumber *size;
@property (nonatomic, readwrite, strong) NSString *fontName;

+ (UIColor *)colorForIndex:(NSInteger)index;
+ (NSNumber *)sizeForIndex:(NSInteger)index;
+ (NSString *)fontNameForIndex:(NSInteger)index;

+ (void)testFonts;

@end
