//
//  LFontSizePalette.m
//  Writeability
//
//  Created by Jelo Agnasin on 11/26/13.
//  Copyright (c) 2013 CNL. All rights reserved.
//

#import "LFontSizePalette.h"

#import "LConstants.h"


#pragma mark - LFontSizePalette Interface
//*********************************************************************************************************************//

@interface LFontSizePalette ()

@property (nonatomic, readwrite, strong) NSArray *sizeButtons;

@property (nonatomic, readwrite, strong) NSArray *sizes;
@property (nonatomic, readwrite, assign) NSInteger rows;
@property (nonatomic, readwrite, assign) NSInteger columns;
@property (nonatomic, readwrite, assign) CGSize size;

@end


//*********************************************************************************************************************//
#pragma mark -
#pragma mark LFontSizePalette
#pragma mark -
//*********************************************************************************************************************//

@implementation LFontSizePalette


#pragma mark - Static Constants
//*********************************************************************************************************************//


#pragma mark - Memory Management
//*********************************************************************************************************************//

- (instancetype)initWithSizes:(NSArray *)sizes
               rows:(NSInteger)rows
            columns:(NSInteger)columns
               size:(CGSize)size
{
    self = [super init];
    if (!self) return nil;
    
    self.sizes = sizes;
    self.rows = rows;
    self.columns = columns;
    self.size = size;
    
    [self setupButtons];

    return self;
}


#pragma mark - Properties
//*********************************************************************************************************************//

- (void)setSelectedIndex:(NSInteger)selectedIndex
{
    if (_selectedIndex != selectedIndex) {
        _selectedIndex = selectedIndex;
        
        [self.sizeButtons enumerateObjectsUsingBlock:^(UIButton *button, NSUInteger idx, BOOL *stop) {
            UIColor *titleColor = selectedIndex == idx ? [UIColor LTealColor] : [UIColor LBlackColor];
            [button setTitleColor:titleColor forState:UIControlStateNormal];
        }];
        
        [self.delegate fontSizePalette:self didChangeIndex:selectedIndex];
    }
}


#pragma mark - Setup Methods
//*********************************************************************************************************************//

- (void)setupButtons
{
    __block CGRect frame = CGRectZero;
    frame.size = self.size;
    
    NSMutableArray *buttons = [NSMutableArray arrayWithCapacity:self.sizes.count];
    [self.sizes enumerateObjectsUsingBlock:^(NSNumber *size, NSUInteger idx, BOOL *stop) {
        UIButton *button = [[UIButton alloc] initWithFrame:frame];
        button.titleLabel.font = [[UIFont LDefaultFont] fontWithSize:size.floatValue];
        
        [button setTitle:@"A" forState:UIControlStateNormal];
        [button setTitleColor:[UIColor LBlackColor] forState:UIControlStateNormal];
        [button addTarget:self action:@selector(didTap:) forControlEvents:UIControlEventTouchUpInside];
        [buttons addObject:button];
        [self addSubview:button];
        
        frame.origin.x = [self computeX:(idx + 1)];
        frame.origin.y = [self computeY:(idx + 1)];
    }];

    [buttons[0] setTitleColor:[UIColor LTealColor] forState:UIControlStateNormal];
    self.sizeButtons = buttons;
}


#pragma mark - Utilities
//*********************************************************************************************************************//

- (CGFloat)computeX:(NSInteger)idx
{
    return (idx % self.columns) * self.size.width;
}

- (CGFloat)computeY:(NSInteger)idx
{
    return (idx / self.columns) * self.size.height;
}


#pragma mark - Actions
//*********************************************************************************************************************//

- (void)didTap:(UIButton *)sender
{
    NSInteger idx = [self.sizeButtons indexOfObject:sender];
    if (idx != NSNotFound) {
        self.selectedIndex = idx;
    }
}

@end
