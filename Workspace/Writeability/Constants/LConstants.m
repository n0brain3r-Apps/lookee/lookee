//
//  LConstants.m
//  Writeability
//
//  Created by Jelo Agnasin on 9/6/13.
//  Copyright (c) 2013 CNL. All rights reserved.
//

#import "LConstants.h"

NSString *const LC__BundleIdentifierDebug   = @"com.lookee.LookeeDev";
NSString *const LC__BundleIdentifierRelease = @"com.lookee.Lookee";

const NSUInteger LC__MaximumDocumentDimension = 792;
const NSUInteger LC__MinimumDocumentDimension = 612;

NSString *const LC_OPEN_IN_FILE     = @"LC_OPEN_IN_FILE";
NSString *const LC_SESSION_JOIN_URL = @"LC_SESSION_JOIN";
NSString *const LC_SESSION_QUIT     = @"LC_SESSION_QUIT";

NSString *const LN__ChangedUserProfile      = @"LN__ChangedUserProfile";
NSString *const LN__DownloadDocumentFailed  = @"LN__DownloadDocumentFailed";
NSString *const LN__UploadDocumentFailed    = @"LN__UploadDocumentFailed";
