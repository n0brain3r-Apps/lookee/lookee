//
//  LAppearanceConstants.h
//  Writeability
//
//  Created by Jelo Agnasin on 10/1/13.
//  Copyright (c) 2013 CNL. All rights reserved.
//

#import <UIKit/UIKit.h>

// constants
extern const CGFloat LC_DEFAULT_MARGIN; // is used for objects that are at the edge of their superview
extern const CGFloat LC_DEFAULT_SPACING; // is used for spacing between two objects with the same hierarchy

// icons
extern NSString *const LC_ATTENDEES_ICON;
extern NSString *const LC_ATTENDEES_TAP_ICON;

extern NSString *const LC_CLEARPAGE_ICON;
extern NSString *const LC_CLEARPAGE_TAP_ICON;

extern NSString *const LC_ENTER_PANEL_ICON;
extern NSString *const LC_ENTER_PANEL_TAP_ICON;

extern NSString *const LC_ERASER_ICON;
extern NSString *const LC_ERASER_TAP_ICON;

extern NSString *const LC_EXPORT_ICON;
extern NSString *const LC_EXPORT_TAP_ICON;

extern NSString *const LC_HILITER_ICON;
extern NSString *const LC_HILITER_TAP_ICON;

extern NSString *const LC_LEFT_PANEL_ICON;
extern NSString *const LC_LEFT_PANEL_TAP_ICON;

extern NSString *const LC_MAGNIFY_PLUS_ICON;
extern NSString *const LC_MAGNIFY_PLUS_TAP_ICON;

extern NSString *const LC_PEN_ICON;
extern NSString *const LC_PEN_TAP_ICON;

extern NSString *const LC_REDO_ICON;
extern NSString *const LC_REDO_TAP_ICON;

extern NSString *const LC_SELECTED_BACKGROUND_IMAGE;

extern NSString *const LC_SETTINGS_ICON;
extern NSString *const LC_SETTINGS_SELECTED_ICON;
extern NSString *const LC_SETTINGS_BACKGROUND_ICON;

extern NSString *const LC_SOFT_LOCK_ICON;
extern NSString *const LC_SOFT_LOCK_TAP_ICON;

extern NSString *const LC_TEXT_ICON;
extern NSString *const LC_TEXT_TAP_ICON;

extern NSString *const LC_UNDO_ICON;
extern NSString *const LC_UNDO_TAP_ICON;

extern NSString *const LC_VISIBILITY_ICON;
extern NSString *const LC_VISIBILITY_TAP_ICON;

// colors
extern NSString *const LC_DEFAULT_TEAL_COLOR;
extern NSString *const LC_DEFAULT_BORDER_COLOR;
extern NSString *const LC_DEFAULT_GRAY_COLOR;
extern NSString *const LC_DEFAULT_DARK_GRAY_COLOR;

extern NSString *const LC_BLACK_COLOR;
extern NSString *const LC_RED_COLOR;
extern NSString *const LC_ORANGE_COLOR;
extern NSString *const LC_YELLOW_COLOR;
extern NSString *const LC_GREEN_COLOR;
extern NSString *const LC_BLUE_COLOR;
extern NSString *const LC_VIOLET_COLOR;
extern NSString *const LC_PINK_COLOR;

// UIFont + LAppearance
@interface UIFont (LAppearance)

+ (UIFont *)LDefaultFont;

@end


// UIColor + LAppearance
@interface UIColor (LAppearance)

+ (UIColor *)LTealColor;
+ (UIColor *)LBorderColor;
+ (UIColor *)LGrayColor;
+ (UIColor *)LDarkGrayColor;

+ (UIColor *)LBlackColor;
+ (UIColor *)LRedColor;
+ (UIColor *)LOrangeColor;
+ (UIColor *)LYellowColor;
+ (UIColor *)LGreenColor;
+ (UIColor *)LBlueColor;
+ (UIColor *)LVioletColor;
+ (UIColor *)LPinkColor;

@end