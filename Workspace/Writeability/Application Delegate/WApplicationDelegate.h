//
//  WApplicationDelegate.h
//  Writeability
//
//  Created by CNL on 7/12/13.
//  Copyright (c) 2013 CNL. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface WApplicationDelegate : UIResponder <UIApplicationDelegate>

@end
