//
//  LWhiteboardContent.m
//  Writeability
//
//  Created by Ryan on 9/15/13.
//  Copyright (c) 2013 CNL. All rights reserved.
//


#import "LPageContent_Private.h"

#import "LPage.h"

#import "LPopupView.h"

#import <Rendering/LGLSurface.h>

#import <Utilities/LMacros.h>
#import <Utilities/LPDF.h>
#import <Utilities/LTimer.h>
#import <Utilities/LDelegateProxy.h>
#import <Utilities/LTouchRecognizer.h>

#import <Utilities/NSObject+Utilities.h>
#import <Utilities/UIColor+Utilities.h>




//*********************************************************************************************************************//
#pragma mark -
#pragma mark Private Classes
#pragma mark -
//*********************************************************************************************************************//




@proxyclass(LScrollViewProxy, UIScrollViewDelegate);




//*********************************************************************************************************************//
#pragma mark -
#pragma mark LPageContent
#pragma mark -
//*********************************************************************************************************************//




@interface LPageContent ()
<
    UIScrollViewDelegate,
    UIGestureRecognizerDelegate,
    LProxyDelegate,
    LGLSurfaceDelegate,
    LPageDelegate
>

@property (nonatomic, readwrite , weak  ) id                receiver;
@property (nonatomic, readonly  , strong) LScrollViewProxy  *proxy;

@property (nonatomic, readonly  , weak  ) UIView            *container;
@property (nonatomic, readonly  , weak  ) LGLSurface        *surface;

@property (nonatomic, readwrite , assign) CGRect            beginZoomRect;

@end


@implementation LPageContent
{
    struct {
        BOOL pageContentDidZoomToRect :1;
    } _delegateRespondsTo;
}

#pragma mark - Constructors/Destructors
//*********************************************************************************************************************//

static const float kLPageContentMinimumZoomScale    = 1.;
static const float kLPageContentMaximumZoomScale    = 7.;

static const float kLPageContentZoomThreshold       = 2.;


#pragma mark - Constructors/Destructors
//*********************************************************************************************************************//

- (instancetype)init __ILLEGALMETHOD__;
- (instancetype)initWithFrame:(CGRect)frame __ILLEGALMETHOD__;

- (instancetype)initWithContent:(LPageContent *)content
{
    DBGParameterAssert(content != nil);

    if ((self = [super initWithFrame:[content bounds]])) {
        {_clone = YES;}

        NSUInteger  width   = [content.surface width];
        NSUInteger  height  = [content.surface height];
        CGFloat     scale   = [content.surface scale];
        NSUInteger  samples = [content.surface samples];

        LGLSurface *viewport = [[LGLSurface alloc]
                                initWithWidth   :width
                                height          :height
                                scale           :scale
                                samples         :samples];

        viewport.delegate           = self;
        viewport.connectedSurface   = content.surface;

        _page           = [content page];

        self.delegate   = [content delegate];

        [content.clones addObject:self];
        [self initializeSurface:viewport];
    }

    return self;
}

- (instancetype)initWithFrame:(CGRect)frame page:(LPage *)page scale:(CGFloat)scale
{
    DBGParameterAssert(page != nil);

	if ((self = [super initWithFrame:frame])) {
        _page           = page;
        _scale          = scale;
        
        _tagsVisible    = YES;

        LGLSurface *surface = [[LGLSurface alloc]
                               initWithWidth   :page.dimensions.width
                               height          :page.dimensions.height
                               scale           :scale
                               samples         :4];

        [page addDelegate:self];
        [surface setDelegate:self];
        
        [self initializeSurface:surface];
	}
    
	return self;
}

- (void)initializeSurface:(LGLSurface *)surface
{
    _zoomEnabled    = YES;
    
    _proxy          = [[LScrollViewProxy alloc] initWithDelegate:self];
    _clones         = [NSHashTable weakObjectsHashTable];

    UIView *overlay     = [[UIView alloc] initWithFrame:[surface bounds]];
    UIView *container   = [[UIView alloc] initWithFrame:[surface bounds]];

    [container setBackgroundColor:[UIColor whiteColor]];
    [container setClipsToBounds:YES];
    [container addSubview:surface];
    [container addSubview:overlay];

    [self addSubview:container];

    _surface    = surface;
    _overlay    = overlay;
    _container  = container;

    super.delegate                      = _proxy;
    self.showsHorizontalScrollIndicator = NO;
    self.showsVerticalScrollIndicator   = NO;
    self.scrollsToTop                   = NO;
    self.delaysContentTouches           = NO;
    self.bounces                        = YES;
    self.decelerationRate               = UIScrollViewDecelerationRateFast;
    self.contentSize                    = container.bounds.size;
    
    self.panGestureRecognizer.minimumNumberOfTouches = 2;
    self.panGestureRecognizer.maximumNumberOfTouches = 2;

    [self scaleContentToFit];
}

- (void)dealloc
{
    DBGMessage(@"deallocated %@", self);
}


#pragma mark - Overridden Methods
//*********************************************************************************************************************//

- (id)copyWithZone:(NSZone *)zone
{
    return [[LPageContent allocWithZone:zone] initWithContent:self];
}

- (void)layoutSubviews
{
	CGSize size		= self.bounds.size;
	CGRect frame 	= [_container frame];

	if (frame.size.width < size.width) {
		frame.origin.x = ((size.width - frame.size.width) *.5 + self.contentOffset.x);
	} else {
		frame.origin.x = 0.;
	}

	if (frame.size.height < size.height) {
		frame.origin.y = ((size.height - frame.size.height) *.5 + self.contentOffset.y);
	} else {
		frame.origin.y = 0.;
	}

	[_container setFrame:frame];
}

- (void)setDelegate:(id)delegate
{
    {_receiver = delegate;}

    _delegateRespondsTo.pageContentDidZoomToRect
    = [delegate respondsToSelector:@selector(pageContent:didZoomToRect:)];

    super.delegate = nil;
    super.delegate = _proxy;
}

- (id)delegate
{
    return _receiver;
}


#pragma mark - Properties
//*********************************************************************************************************************//

@synthesize zoomEnabled = _zoomEnabled;

@dynamic zoomRect;

- (CGRect)zoomRect
{
    return [self convertRect:self.bounds toView:_container];
}


#pragma mark - Public Methods
//*********************************************************************************************************************//

- (void)scaleContentToFit
{
    [self scaleContentToFitSize:self.bounds.size];
}

- (void)scaleContentToFitSize:(CGSize)size
{
    CGRect 	sourceRect          = [_container bounds];

    CGFloat minimumZoomScale    = [self minimumZoomScale];

	CGFloat zoomScaleX          = (size.width  / sourceRect.size.width);
	CGFloat zoomScaleY          = (size.height / sourceRect.size.height);

	CGFloat zoomScale           = fminf(zoomScaleX, zoomScaleY);

	[self setMinimumZoomScale:(zoomScale * kLPageContentMinimumZoomScale)];
	[self setMaximumZoomScale:(zoomScale * kLPageContentMaximumZoomScale)];

    if (IsEqual([self zoomScale], minimumZoomScale)) {
        [self setZoomScale:[self minimumZoomScale]];
    } else if (isless([self zoomScale], [self minimumZoomScale])) {
        [self setZoomScale:[self minimumZoomScale]];
    } else if (isgreater([self zoomScale], [self maximumZoomScale])) {
        [self setZoomScale:[self maximumZoomScale]];
    }
}

- (void)scaleContentToFitWidth:(CGFloat)width
{
    [self scaleContentToFitSize:((CGSize){width, INFINITY})];
}


#pragma mark - UIScrollViewDelegate
//*********************************************************************************************************************//

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return [self container];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if (!(decelerate) && ![self isClone]) {
        if (_delegateRespondsTo.pageContentDidZoomToRect) {
            [self.receiver pageContent:self didZoomToRect:[self zoomRect]];
        }
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if (![self isClone]) {
        if (_delegateRespondsTo.pageContentDidZoomToRect) {
            [self.receiver pageContent:self didZoomToRect:[self zoomRect]];
        }
    }
}

- (void)scrollViewDidEndZooming:(UIScrollView *)scrollView withView:(UIView *)view atScale:(CGFloat)scale
{
    if (![self isClone]) {
        if (_delegateRespondsTo.pageContentDidZoomToRect) {
            [self.receiver pageContent:self didZoomToRect:[self zoomRect]];
        }
    }
}


#pragma mark - LGLViewportDelegate
//*********************************************************************************************************************//

- (void)surface:(LGLSurface *)surface renderInRect:(CGRect)rect
{
    [self.surface clear];
	[self.surface queueRenderOperation:^{
		[self.page renderWithScale:self.scale];
	}];
}


#pragma mark - LPageDelegate
//*********************************************************************************************************************//

- (void)pageDidUpdate:(LPage *)page
{
    [self.surface setNeedsDisplay];
}

- (void)
pageDidMoveTool :(LPage *   )page
withColor       :(UIColor * )color
toLocation      :(CGPoint   )location
atAnnotatorIndex:(NSUInteger)index
{
    if ((index > 0) && [self areTagsVisible]) {
        LTimer *$sub(timer, id object) {
            return [object associatedObject];
        };

        UIView *pointer = [self.surface viewWithTag:index];

        if ((pointer != nil) && ![pointer.backgroundColor isEqualToColor:color]) {
            [timer(pointer) expire];

            [pointer setTag:-1];
            (pointer = nil);
        }

        if (!pointer) {
            pointer = [UIView new];
            [pointer setTag:index];
            [pointer setCenter:location];
            [pointer setBackgroundColor:color];

            if (isgreater([color alpha], .1)) {
                [pointer setAlpha:.3];

                [pointer.layer setBorderWidth:0.];
            } else {
                color = [color colorWithAlphaComponent:1.];

                [pointer.layer setBorderWidth:2.];
                [pointer.layer setBorderColor:color.CGColor];
            }

            [pointer.layer setCornerRadius:16.];

            LPopupView *popup = [LPopupView new];
            [popup setPreferredPointDirection:kLPopupPointDirectionDown];
            [popup setAnimation:kLPopupAnimationSlide];
            [popup setBackgroundColor:color];
            [popup setBorderColor:color];
            [popup setCornerRadius:4.];
            [popup setPointerSize:4.];
            [popup setTextColor:[color isLight]?[UIColor blackColor]:[UIColor whiteColor]];

            id(^nameBlock)(void) = [self.page tagForAnnotatorAtIndex:index];

            [popup setText:nameBlock()];

            [self.surface addSubview:pointer];

            [pointer observerForKeyPath:@Keypath(pointer.center)] (^{
                [popup updateFrameAnimated:YES];
            });

            $weakify(pointer) {
                [pointer setAssociatedObject:
                 [LTimer timerWithTimeInterval:.5 block:^{
                    $strongify(pointer) {
                        [UIView animateWithDuration:.3 animations:^{
                            [popup dismissAnimated:YES];
                            [pointer setBounds:CGRectZero];
                        } completion:^(BOOL finished) {
                            [pointer removeFromSuperview];
                        }];
                    }
                }]];
            }
            
            [UIView animateWithDuration:.3 animations:^{
                [pointer setBounds:((CGRect){{0}, {32., 32.}})];
                [popup presentPointingAtView:pointer inView:self animated:YES];
            } completion:^(BOOL finished) {
                [timer(pointer) start];
            }];
        } else {
            [pointer setCenter:location];

            [timer(pointer) start];
        }
    }
}


#pragma mark - UIGestureRecognizerDelegate
//*********************************************************************************************************************//

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    if ([self pinchGestureRecognizer] == gestureRecognizer) {
        return [self isZoomEnabled];
    }

    if ([self panGestureRecognizer] == gestureRecognizer) {
        return [self isZoomEnabled] && !IsEqual([self zoomScale], [self minimumZoomScale]);
    }

    return YES;
}

@end
