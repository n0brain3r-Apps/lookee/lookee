//
//  LWhiteboardContent.h
//  Writeability
//
//  Created by Ryan on 9/15/13.
//  Copyright (c) 2013 CNL. All rights reserved.
//


// TODO: Use forwarding instead of runtime tricks to implement plugin architecture


#import "LPage.h"



@interface LPageContent : UIScrollView <NSCopying>

@property (nonatomic, readonly  , strong) LPage     *page;

@property (nonatomic, readonly  , assign) CGFloat   scale;

@property (nonatomic, readonly  , assign) CGRect    zoomRect;

@property (nonatomic, readonly  , strong) UIView    *overlay;

@property (nonatomic, readwrite , weak  ) id        delegate;

@property (nonatomic, readwrite , assign, getter=isZoomEnabled  ) BOOL zoomEnabled;

@property (nonatomic, readwrite , assign, getter=areTagsVisible ) BOOL tagsVisible;


- (instancetype)initWithFrame:(CGRect)frame page:(LPage *)page scale:(CGFloat)scale;


- (void)scaleContentToFit;
- (void)scaleContentToFitSize:(CGSize)size;
- (void)scaleContentToFitWidth:(CGFloat)width;

@end



// !!!: We use an informal protocol here to make it easy to extend
@interface NSObject (LPageContentDelegate)

- (void)pageContent:(LPageContent *)content didZoomToRect:(CGRect)rect;

@end