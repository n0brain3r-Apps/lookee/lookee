//
//  LPageLoader.h
//  Writeability
//
//  Created by Ryan Blonna on 30/10/14.
//  Copyright (c) 2014 CNL. All rights reserved.
//


#import <UIKit/UIKit.h>




@class LPage;




@interface LPageLoader : UIView

@property (nonatomic, readonly, strong) LPage   *page;

@property (nonatomic, readonly, assign) CGFloat scale;


- (instancetype)initWithFrame:(CGRect)frame page:(LPage *)page scale:(CGFloat)scale;

@end
