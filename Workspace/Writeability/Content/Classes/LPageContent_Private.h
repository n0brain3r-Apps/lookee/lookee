//
//  LWhiteboardContent_Private.h
//  Writeability
//
//  Created by Ryan on 2/27/14.
//  Copyright (c) 2014 CNL. All rights reserved.
//


#import "LPageContent.h"


@interface LPageContent ()

@property (nonatomic, readonly  , assign, getter = isClone) BOOL clone;

@property (nonatomic, readonly  , strong) NSHashTable       *clones;

@end
