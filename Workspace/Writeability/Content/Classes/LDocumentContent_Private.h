//
//  LDocumentContent_Private.h
//  Writeability
//
//  Created by Ryan on 4/14/14.
//  Copyright (c) 2014 CNL. All rights reserved.
//


#import "LDocumentContent.h"

#import "LPageContent+Stroke.h"

#import "LPageContent+Text.h"



@interface LDocumentContent ()

@property (nonatomic, readonly  , weak) UIView          *detail;

@property (nonatomic, readwrite , weak) LPageContent    *pageContent;


- (void)
setShowDetail   :(BOOL          )showDetail
animated        :(BOOL          )animated
invoke          :(void(^)(void) )invoke
finish          :(void(^)(void) )finish;

@end
