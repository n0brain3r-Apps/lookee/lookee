//
//  LPageContent+Stroke.h
//  Writeability
//
//  Created by Ryan on 3/4/14.
//  Copyright (c) 2014 CNL. All rights reserved.
//


#import "LPageContent.h"


@interface LPageContent (Stroke)

@property (nonatomic, readwrite , assign, getter = isStrokeContentEnabled) BOOL strokeContentEnabled;


- (void)initializeStrokeContent;

@end


@interface NSObject (LPageContentDelegate_Stroke)

- (void)pageContent:(LPageContent *)content didBeginAnnotationAtPoint:(CGPoint)point;
- (void)pageContent:(LPageContent *)content didMoveAnnotationToPoint:(CGPoint)point;
- (void)pageContent:(LPageContent *)content didEndAnnotationAtPoint:(CGPoint)point;

@end