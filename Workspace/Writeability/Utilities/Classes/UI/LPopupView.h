//
//  LPopupView.h
//  Writeability
//
//  Created by Ryan on 12/4/13.
//  Copyright (c) 2013 CNL. All rights reserved.
//


#import <UIKit/UIKit.h>


typedef enum {
    kLPopupPointDirectionAny,
    kLPopupPointDirectionUp,
    kLPopupPointDirectionDown,
} LPopupPointDirection;

typedef enum {
    kLPopupAnimationSlide,
    kLPopupAnimationPop
} LPopupAnimation;


@interface LPopupView : UIView

@property (nonatomic, readwrite , strong) void                  (^userDismissVerb)(void);

@property (nonatomic, readwrite , assign) BOOL                  disableTapToDismiss;
@property (nonatomic, readwrite , assign) BOOL                  shouldDismissOnTapAnywhere;

@property (nonatomic, readonly  , assign, getter=isPresented)   BOOL presented;

@property (nonatomic, readwrite , assign) BOOL                  hasShadow;
@property (nonatomic, readwrite , assign) BOOL                  hasGradientBackground;

@property (nonatomic, readwrite , assign) LPopupAnimation       animation;

@property (nonatomic, readwrite , assign) CGFloat               maximumWidth;
@property (nonatomic, readwrite , assign) CGFloat               sidePadding;
@property (nonatomic, readwrite , assign) CGFloat               topMargin;

@property (nonatomic, readwrite , assign) CGFloat               cornerRadius;
@property (nonatomic, readwrite , assign) CGFloat               borderWidth;
@property (nonatomic, readwrite , strong) UIColor               *borderColor;

@property (nonatomic, readwrite , strong) UIColor               *backgroundColor;

@property (nonatomic, readwrite , strong) NSString              *text;
@property (nonatomic, readwrite , strong) UIFont                *textFont;
@property (nonatomic, readwrite , strong) UIColor               *textColor;
@property (nonatomic, readwrite , assign) NSTextAlignment       textAlignment;

@property (nonatomic, readwrite , strong) UIView                *customView;

@property (nonatomic, readonly  , strong) UIView                *targetView;

@property (nonatomic, readonly  , assign) LPopupPointDirection  pointDirection;
@property (nonatomic, readwrite , assign) LPopupPointDirection  preferredPointDirection;
@property (nonatomic, readwrite , assign) CGFloat               pointerSize;


- (instancetype)initWithText:(NSString *)text;
- (instancetype)initWithCustomView:(UIView *)view;

- (void)presentPointingAtView:(UIView *)targetView inView:(UIView *)containerView animated:(BOOL)animated;
- (void)updateFrameAnimated:(BOOL)animated;

- (void)dismissAnimated:(BOOL)animated;
- (void)autoDismissAnimated:(BOOL)animated atTimeInterval:(NSTimeInterval)timeInvertal;

@end