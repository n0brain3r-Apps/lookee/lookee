//
//  LLineLayer.h
//  Writeability
//
//  Created by Jelo Agnasin on 10/9/13.
//  Copyright (c) 2013 CNL. All rights reserved.
//


#import "LLayer.h"


extern const CGFloat kLineLayerHeight;


@interface LLineLayer : LLayer

@end
