//
//  LPopoverBackgroundView.h
//  Writeability
//
//  Created by Jelo Agnasin on 10/3/13.
//  Copyright (c) 2013 CNL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LPopoverBackgroundView : UIPopoverBackgroundView

@end
