//
//  LSlider.h
//  Writeability
//
//  Created by Ryan Blonna on 24/9/14.
//  Copyright (c) 2014 CNL. All rights reserved.
//


#import <UIKit/UIKit.h>



@protocol LSliderDelegate;



@interface LSlider : UIView

@property (nonatomic, readwrite , assign) CGFloat   trackWidth;

@property (nonatomic, readwrite , assign) CGSize    thumbSize;

@property (nonatomic, readwrite , strong) UIColor   *sliderColor;

@property (nonatomic, readwrite , strong) UIFont    *sliderFont;

@property (nonatomic, readwrite , assign) NSRange   range;

@property (nonatomic, readwrite , assign) NSInteger value;

@property (nonatomic, readwrite , weak  ) id        <LSliderDelegate>delegate;


- (void)showNotificationsInView:(UIView *)view;


- (void)setValue:(NSInteger)value animated:(BOOL)animated;

@end


@protocol LSliderDelegate <NSObject>

@required
- (void)sliderDidCancelValueChange:(LSlider *)slider;
- (void)slider:(LSlider *)slider didChangeToValue:(NSInteger)value;


@optional
- (id)sliderNotificationForValue:(NSInteger)value;

@end
