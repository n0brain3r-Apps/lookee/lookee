//
//  LTextView.m
//  Writeability
//
//  Created by Ryan on 2/18/14.
//  Copyright (c) 2014 CNL. All rights reserved.
//


#import "LTextView.h"

#import <Utilities/LMacros.h>
#import <Utilities/LDelegateProxy.h>




//*********************************************************************************************************************//
#pragma mark -
#pragma mark Private Classes
#pragma mark -
//*********************************************************************************************************************//




@proxyclass(LTextViewProxy, UITextViewDelegate);




//*********************************************************************************************************************//
#pragma mark -
#pragma mark LTextView
#pragma mark -
//*********************************************************************************************************************//





@interface LTextView () <LProxyDelegate>

@property (nonatomic, readwrite , weak  ) id                <UITextViewDelegate>receiver;
@property (nonatomic, readonly  , strong) LTextViewProxy    *proxy;

@end

@implementation LTextView

#pragma mark - Constructors/Destructors
//*********************************************************************************************************************//

- (instancetype)initWithFrame:(CGRect)frame
{
    if ((self = [super initWithFrame:frame])) {
        _proxy = [LTextViewProxy.alloc initWithDelegate:self];

        [super setDelegate:_proxy];
    }

    return self;
}


#pragma mark - Overridden Methods
//*********************************************************************************************************************//

- (void)drawRect:(CGRect)rect
{
    [super drawRect:rect];

    if ([self placeholderText] && ![self hasText]) {
        UIColor *placeholderColor = (_placeholderColor?:[UIColor lightGrayColor]);

        if ([self respondsToSelector:@selector(snapshotViewAfterScreenUpdates:)]) {
            NSMutableParagraphStyle *paragraphStyle = [NSMutableParagraphStyle new];
            paragraphStyle.alignment = [self textAlignment];

            [[self placeholderText]
             drawInRect:((CGRect) {
                .origin = {
                    .x = 5.,
                    .y = (self.contentInset.top +8.),
                },
                .size   = {
                    .width  = (self.frame.size.width  - self.contentInset.left),
                    .height = (self.frame.size.height - self.contentInset.top)
                }
            })
             withAttributes:@{NSFontAttributeName           :[self font],
                              NSForegroundColorAttributeName:placeholderColor,
                              NSParagraphStyleAttributeName :paragraphStyle}];
        } else {
            [placeholderColor set];

            [[self placeholderText]
             drawInRect:((CGRect) {
                .origin = {
                    .x = 8.,
                    .y = 8.,
                },
                .size   = {
                    .width  = self.frame.size.width  -16.,
                    .height = self.frame.size.height -16.
                }
            })
             withFont:self.font];
        }
    }
}

- (void)setText:(NSString *)text
{
    BOOL scrollEnabled = [self isScrollEnabled];

    [self setScrollEnabled:YES];
    [super setText:text];
    [self setScrollEnabled:scrollEnabled];
}

- (void)setAttributedText:(NSAttributedString *)attributedText
{
    BOOL scrollEnabled = [self isScrollEnabled];

    [self setScrollEnabled:YES];
    [super setAttributedText:attributedText];
    [self setScrollEnabled:scrollEnabled];
}

- (void)setContentOffset:(CGPoint)offset
{
	if ([self isTracking] || [self isDecelerating]) {
        UIEdgeInsets insets = [self contentInset];
        insets.bottom   = 0;
        insets.top      = 0;

        [self setContentInset:insets];
	} else {
		float bottomOffset = (self.contentSize.height -
                              self.frame.size.height +
                              self.contentInset.bottom);

		if(isless(offset.y, bottomOffset) && [self isScrollEnabled]){
            UIEdgeInsets insets = self.contentInset;
            insets.bottom       = 8.;
            insets.top          = 0.;

            [self setContentInset:insets];
        }
	}

    if (isgreater(offset.y,
                  (self.contentSize.height -
                   self.frame.size.height))
        &&
        ![self isDecelerating] &&
        ![self isTracking] &&
        ![self isDragging]) {
        offset = CGPointMake(offset.x, self.contentSize.height - self.frame.size.height);
    }

	[super setContentOffset:offset];
}

- (void)setContentInset:(UIEdgeInsets)insets
{
	if (isgreater(insets.bottom, 8)) {
        insets.bottom = 0;
    }

	insets.top = 0;

	[super setContentInset:insets];
}

- (void)setContentSize:(CGSize)contentSize
{
    if (isgreater(self.contentSize.height, contentSize.height)) {
        UIEdgeInsets insets = [self contentInset];
        insets.bottom   = 0.;
        insets.top      = 0.;

        [self setContentInset:insets];
    }

    [super setContentSize:contentSize];
}

- (BOOL)canPerformAction:(SEL)action withSender:(id)sender
{
    if (action == @selector(select:) ||
        action == @selector(selectAll:) ||
        action == @selector(paste:) ||
        action == @selector(cut:) ||
        action == @selector(copy:) ||
        action == @selector(delete:)) {
        return [super canPerformAction:action withSender:sender];
    }

    return NO;
}


#pragma mark - Properties
//*********************************************************************************************************************//

- (void)setPlaceholderText:(NSString *)placeholderText
{
	{_placeholderText = placeholderText;}

	[self setNeedsDisplay];
}

- (void)setPlaceholderColor:(UIColor *)placeholderColor
{
    {_placeholderColor = placeholderColor;}

	[self setNeedsDisplay];
}

- (void)setDelegate:(id <UITextViewDelegate>)delegate
{
    {_receiver = delegate;}

    super.delegate = nil;
    super.delegate = _proxy;
}

- (id <UITextViewDelegate>)delegate
{
    return _receiver;
}


#pragma mark - UITextViewDelegate
//*********************************************************************************************************************//

- (BOOL)
textView                :(UITextView *  )textView
shouldChangeTextInRange :(NSRange       )range
replacementText         :(NSString *    )text
{
	if(![self hasText] && [text isEqualToString:@""]) {
        return NO;
    }

	return YES;
}

- (void)textViewDidChange:(UITextView *)textView
{
    NSAttributedString  *text       = [[NSAttributedString alloc]
                                       initWithString   :textView.text
                                       attributes       :self.typingAttributes];

    NSRange             selected    = [textView selectedRange];
    [textView setAttributedText:text];
    [textView setSelectedRange:selected];
}

@end
