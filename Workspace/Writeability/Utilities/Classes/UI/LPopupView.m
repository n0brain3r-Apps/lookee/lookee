//
//  LPopupView.m
//  Writeability
//
//  Created by Ryan on 12/4/13.
//  Copyright (c) 2013 CNL. All rights reserved.
//


#import "LPopupView.h"

#import "LConstants.h"

#import <Utilities/LMacros.h>
#import <Utilities/LTimer.h>

#import <Utilities/NSObject+Utilities.h>
#import <Utilities/UIColor+Utilities.h>





//*********************************************************************************************************************//
#pragma mark -
#pragma mark LPopupView
#pragma mark -
//*********************************************************************************************************************//





@interface LPopupView ()

@property (nonatomic, readwrite , assign) CGPoint               targetPoint;
@property (nonatomic, readwrite , assign) LPopupPointDirection  pointDirection;

@property (nonatomic, readwrite , assign) BOOL                  shouldHighlight;

@property (nonatomic, readonly  , assign) CGRect                bubbleFrame;
@property (nonatomic, readwrite , assign) CGSize                bubbleSize;

@property (nonatomic, readwrite , strong) UIView                *targetView;
@property (nonatomic, readwrite , strong) UIButton              *dismissTarget;
@property (nonatomic, readwrite , weak  ) UIView                *presentedView;

@property (nonatomic, readwrite , strong) LTimer                *autoDismissTimer;

@property (nonatomic, readwrite , assign) CGSize                previousFrameSize;
@property (nonatomic, readwrite , assign) CGPoint               previousTargetPoint;

@property (nonatomic, readwrite , assign, getter=isPresented)   BOOL presented;

@end


@implementation LPopupView

#pragma mark - Constructors/Destructors
//*********************************************************************************************************************//

- (instancetype)initWithFrame:(CGRect)frame
{
    if ((self = [super initWithFrame:frame])) {
        [self setOpaque:NO];

        [self setTopMargin:2.];
        [self setPointerSize:12.];
        [self setSidePadding:2.];
        [self setBorderWidth:0.];
        [self setTextFont:[[UIFont LDefaultFont] fontWithSize:18.]];
        [self setTextColor:[UIColor whiteColor]];
        [self setTextAlignment:NSTextAlignmentCenter];
        [self setBackgroundColor:[UIColor LTealColor]];
        [self setBorderColor:[UIColor whiteColor]];
        [self setHasShadow:NO];
        [self setAnimation:kLPopupAnimationSlide];
        [self setShouldDismissOnTapAnywhere:NO];
        [self setPreferredPointDirection:kLPopupPointDirectionAny];
        [self setHasGradientBackground:NO];
        [self setCornerRadius:12.];

        $weakify(self) {
            [self observerForKeyPaths:
             (@[ @Keypath(self.hasGradientBackground),
                 @Keypath(self.backgroundColor      ),
                 @Keypath(self.borderColor          ) ])] (^{
                 $strongify(self) {
                     [self setNeedsDisplay];
                 }
             });

            [self observerForKeyPaths:
             (@[ @Keypath(self.targetView) ])] (^{
                $strongify(self) {
                    [self updateFrameAnimated:YES];
                }
            });
        }
    }
    
    return self;
}

- (instancetype)initWithText:(NSString *)text
{
    if ((self = [self initWithFrame:CGRectZero])) {
        [self setText:text];
    }

    return self;
}

- (instancetype)initWithCustomView:(UIView *)view
{
    if ((self = [self initWithFrame:CGRectZero])) {
        [self setCustomView:view];
        [self addSubview:self.customView];
    }
    
    return self;
}


#pragma mark - Overridden Methods
//*********************************************************************************************************************//

- (void)drawRect:(CGRect)rect
{
    CGRect bubbleRect = CGRectIntegral([self bubbleFrame]);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetStrokeColorWithColor(context, [UIColor blackColor].CGColor);

    CGMutablePathRef bubblePath = CGPathCreateMutable();

    if ([self pointDirection] == kLPopupPointDirectionUp) {
        CGPathMoveToPoint(bubblePath, NULL,
                          self.targetPoint.x + self.sidePadding,
                          self.targetPoint.y);
        
        CGPathAddLineToPoint(bubblePath, NULL,
                             self.targetPoint.x + self.sidePadding + self.pointerSize,
                             self.targetPoint.y + self.pointerSize);
        
        CGPathAddArcToPoint(bubblePath, NULL,
                            bubbleRect.origin.x + bubbleRect.size.width,
                            bubbleRect.origin.y,
                            bubbleRect.origin.x + bubbleRect.size.width,
                            bubbleRect.origin.y + self.cornerRadius,
                            self.cornerRadius);

        CGPathAddArcToPoint(bubblePath, NULL,
                            bubbleRect.origin.x + bubbleRect.size.width,
                            bubbleRect.origin.y + bubbleRect.size.height,
                            bubbleRect.origin.x + bubbleRect.size.width - self.cornerRadius,
                            bubbleRect.origin.y + bubbleRect.size.height,
                            self.cornerRadius);

        CGPathAddArcToPoint(bubblePath, NULL,
                            bubbleRect.origin.x,
                            bubbleRect.origin.y + bubbleRect.size.height,
                            bubbleRect.origin.x,
                            bubbleRect.origin.y + bubbleRect.size.height - self.cornerRadius,
                            self.cornerRadius);

        CGPathAddArcToPoint(bubblePath, NULL,
                            bubbleRect.origin.x,
                            bubbleRect.origin.y,
                            bubbleRect.origin.x + self.cornerRadius,
                            bubbleRect.origin.y,
                            self.cornerRadius);

        CGPathAddLineToPoint(bubblePath, NULL,
                             self.targetPoint.x + self.sidePadding - self.pointerSize,
                             self.targetPoint.y + self.pointerSize);
    } else {
        CGPathMoveToPoint(bubblePath, NULL,
                          self.targetPoint.x + self.sidePadding,
                          self.targetPoint.y);

        CGPathAddLineToPoint(bubblePath, NULL,
                             self.targetPoint.x + self.sidePadding - self.pointerSize,
                             self.targetPoint.y - self.pointerSize);
        
        CGPathAddArcToPoint(bubblePath, NULL,
                            bubbleRect.origin.x,
                            bubbleRect.origin.y + bubbleRect.size.height,
                            bubbleRect.origin.x,
                            bubbleRect.origin.y + bubbleRect.size.height - self.cornerRadius,
                            self.cornerRadius);

        CGPathAddArcToPoint(bubblePath, NULL,
                            bubbleRect.origin.x,
                            bubbleRect.origin.y,
                            bubbleRect.origin.x + self.cornerRadius,
                            bubbleRect.origin.y,
                            self.cornerRadius);

        CGPathAddArcToPoint(bubblePath, NULL,
                            bubbleRect.origin.x + bubbleRect.size.width,
                            bubbleRect.origin.y,
                            bubbleRect.origin.x + bubbleRect.size.width,
                            bubbleRect.origin.y + self.cornerRadius,
                            self.cornerRadius);

        CGPathAddArcToPoint(bubblePath, NULL,
                            bubbleRect.origin.x + bubbleRect.size.width,
                            bubbleRect.origin.y + bubbleRect.size.height,
                            bubbleRect.origin.x + bubbleRect.size.width - self.cornerRadius,
                            bubbleRect.origin.y + bubbleRect.size.height,
                            self.cornerRadius);

        CGPathAddLineToPoint(bubblePath, NULL,
                             self.targetPoint.x + self.sidePadding + self.pointerSize,
                             self.targetPoint.y - self.pointerSize);
    }
    
    CGPathCloseSubpath(bubblePath);

    CGContextSaveGState(context);
    CGContextAddPath(context, bubblePath);
    CGContextClip(context);

    if (![self hasGradientBackground]) {
        CGContextSetFillColorWithColor(context, [self backgroundColor].CGColor);
        CGContextFillRect(context, [self bounds]);
    } else {
        CGFloat bubbleMiddle = (bubbleRect.origin.y + (bubbleRect.size.height /2.)) / [self bounds].size.height;
        
        CGGradientRef   myGradient;
        CGColorSpaceRef myColorSpace;

        size_t  locationCount   = 5;
        CGFloat locationList[]  = {0., bubbleMiddle -.03, bubbleMiddle, bubbleMiddle +.03, 1.};
        
        CGFloat colorHL = 0.;

        if ([self shouldHighlight]) {
            colorHL = .25;
        }

        CGFloat components[4]; [self.backgroundColor getComponents:components];

        CGFloat red     = components[0];
        CGFloat green   = components[1];
        CGFloat blue    = components[2];
        CGFloat alpha   = components[3];

        CGFloat colorList[] = {
            red *1.16   + colorHL, green *1.16  + colorHL, blue *1.16   + colorHL, alpha,
            red *1.16   + colorHL, green *1.16  + colorHL, blue *1.16   + colorHL, alpha,
            red *1.08   + colorHL, green *1.08  + colorHL, blue *1.08   + colorHL, alpha,
            red         + colorHL, green        + colorHL, blue         + colorHL, alpha,
            red         + colorHL, green        + colorHL, blue         + colorHL, alpha
        };

        myColorSpace    = CGColorSpaceCreateDeviceRGB();
        myGradient      = CGGradientCreateWithColorComponents(myColorSpace, colorList, locationList, locationCount);

        CGPoint startPoint;
        startPoint.x = 0;
        startPoint.y = 0;

        CGPoint endPoint;
        endPoint.x = 0;
        endPoint.y = CGRectGetMaxY([self bounds]);
        
        CGContextDrawLinearGradient(context, myGradient, startPoint, endPoint,0);
        CGGradientRelease(myGradient);
        CGColorSpaceRelease(myColorSpace);
    }

    CGContextRestoreGState(context);

    if (isgreater([self borderWidth], 0.)) {
        CGContextSaveGState(context);

        CGContextSetStrokeColorWithColor(context, [self borderColor].CGColor);

        CGRect frame    = [self bubbleFrame];

        CGRect bounds   = CGRectIntegral((CGRect) {
            .origin = {
                .x = CGRectGetMinX(frame) + [self borderWidth],
                .y = CGRectGetMinY(frame) + [self borderWidth]
            },
            .size   = {
                .width  = CGRectGetWidth(frame) + [self borderWidth],
                .height = CGRectGetHeight(frame) + [self borderWidth]
            }
        });

        CGPathRef boundsPath = CGPathCreateWithRoundedRect(bounds, self.cornerRadius, self.cornerRadius, NULL);

        CGContextAddPath(context, bubblePath);
        CGContextAddPath(context, boundsPath);
        CGContextDrawPath(context, kCGPathEOFill);

        CGPathRelease(boundsPath);

        CGContextRestoreGState(context);
    }
    
    CGPathRelease(bubblePath);
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    if ([self disableTapToDismiss]) {
        [super touchesBegan:touches withEvent:event];
        return;
    }

    [self dismissByUser];
}


#pragma mark - Properties
//*********************************************************************************************************************//

- (CGRect)bubbleFrame
{
    if (self.pointDirection == kLPopupPointDirectionUp) {
        return ((CGRect) {
            .origin = {
                .x = self.sidePadding,
                .y = self.targetPoint.y + self.pointerSize
            },
            .size   = self.bubbleSize
        });
    }

    return ((CGRect) {
        .origin = {
            .x = self.sidePadding,
            .y = self.targetPoint.y - self.pointerSize - self.bubbleSize.height
        },
        .size   = self.bubbleSize
    });
}

- (void)setHasShadow:(BOOL)hasShadow
{
    if (_hasShadow != hasShadow) {
        {_hasShadow = hasShadow;}

        if (hasShadow) {
            [self.layer setShadowOffset:((CGSize){.width = 0., .height = 3.})];
            [self.layer setShadowRadius:2.];
            [self.layer setShadowColor:[UIColor blackColor].CGColor];
            [self.layer setShadowOpacity:.3];
        } else {
            [self.layer setShadowOpacity:0.];
        }
    }
}


#pragma mark - Public Methods
//*********************************************************************************************************************//

- (void)presentPointingAtView:(UIView *)targetView inView:(UIView *)containerView animated:(BOOL)animated
{
    [self setTargetView:targetView];

    if (containerView && (self.superview != containerView)) {
        [self.dismissTarget removeFromSuperview];
        [self removeFromSuperview];

        if ([self shouldDismissOnTapAnywhere]) {
            [self setDismissTarget:[UIButton buttonWithType:UIButtonTypeCustom]];
            [self.dismissTarget addTarget:self action:@selector(dismissByUser) forControlEvents:UIControlEventTouchUpInside];
            [self.dismissTarget setTitle:@"" forState:UIControlStateNormal];
            [self.dismissTarget setFrame:containerView.bounds];

            [containerView addSubview:self.dismissTarget];
        }
        
        [containerView addSubview:self];
    }

    [self updateText];
    [self updateBubbleSize];

    [self updateFrameAnimated:animated];

    [self updatePresentedView];

    [self setPresented:YES];
}

- (void)updateFrameAnimated:(BOOL)animated
{
    UIView *targetView      = [self targetView];
    UIView *containerView   = [self superview];

    if (targetView && containerView) {
        [self.autoDismissTimer start];

        UIView *superview       = [containerView superview];

        if ([superview isKindOfClass:[UIWindow class]]) {
            superview = containerView;
        }

        CGFloat fullWidth               = [self bubbleSize].width  + [self sidePadding] *2.;
        CGFloat fullHeight              = [self bubbleSize].height + [self pointerSize];

        CGPoint targetRelativeOrigin    = [targetView.superview convertPoint:targetView.frame.origin toView:superview];
        CGPoint containerRelativeOrigin = [superview convertPoint:containerView.frame.origin toView:superview];

        CGFloat pointerY;

        if (isless(targetRelativeOrigin.y + targetView.bounds.size.height, containerRelativeOrigin.y)) {
            pointerY = 0.;

            [self setPointDirection:kLPopupPointDirectionUp];
        } else if (isgreater(targetRelativeOrigin.y, containerRelativeOrigin.y + containerView.bounds.size.height)) {
            pointerY = [containerView bounds].size.height;

            [self setPointDirection:kLPopupPointDirectionDown];
        } else {
            [self setPointDirection:self.preferredPointDirection];

            CGPoint targetOriginInContainer = [targetView convertPoint:CGPointZero toView:containerView];
            CGFloat sizeBelow               = (containerView.bounds.size.height - targetOriginInContainer.y);

            if (self.pointDirection == kLPopupPointDirectionAny) {
                if (isgreater(sizeBelow, targetOriginInContainer.y)) {
                    pointerY = targetOriginInContainer.y + targetView.bounds.size.height;

                    [self setPointDirection:kLPopupPointDirectionUp];
                } else {
                    pointerY = targetOriginInContainer.y;

                    [self setPointDirection:kLPopupPointDirectionDown];
                }
            } else if (self.pointDirection == kLPopupPointDirectionDown) {
                pointerY = targetOriginInContainer.y;
            } else {
                pointerY = targetOriginInContainer.y + targetView.bounds.size.height;
            }
        }

        CGFloat width   = containerView.bounds.size.width;

        CGPoint point   = [targetView.superview convertPoint:targetView.center toView:containerView];
        CGFloat xCoord  = point.x;
        CGFloat bubbleX = xCoord - roundf(self.bubbleSize.width /2.);

        if (isless(bubbleX, [self sidePadding])) {
            bubbleX = [self sidePadding];
        }

        if (isgreater(bubbleX + self.bubbleSize.width + self.sidePadding, width)) {
            bubbleX = width - self.bubbleSize.width - self.sidePadding;
        }

        if (isless(xCoord - self.pointerSize, bubbleX + self.cornerRadius)) {
            xCoord = bubbleX + self.cornerRadius + self.pointerSize;
        }

        if (isgreater(xCoord + self.pointerSize, bubbleX + self.bubbleSize.width - self.cornerRadius)) {
            xCoord = bubbleX + self.bubbleSize.width - self.cornerRadius - self.pointerSize;
        }

        CGFloat bubbleY;

        if ([self pointDirection] == kLPopupPointDirectionUp) {
            bubbleY = [self topMargin] + pointerY;

            [self setTargetPoint:((CGPoint) {
                .x = xCoord - bubbleX,
                .y = 0.
            })];
        } else {
            bubbleY = pointerY - fullHeight;

            [self setTargetPoint:((CGPoint) {
                .x = xCoord - bubbleX,
                .y = fullHeight -2.
            })];
        }

        CGRect finalFrame = ((CGRect) {
            .origin = {
                .x = bubbleX - [self sidePadding],
                .y = bubbleY
            },
            .size   = {
                .width  = fullWidth,
                .height = fullHeight
            }
        });

        finalFrame = ((CGRect) {
            .origin = {
                .x = floorf(finalFrame.origin.x),
                .y = floorf(finalFrame.origin.y)
            },
            .size   = {
                .width  = roundf(finalFrame.size.width),
                .height = roundf(finalFrame.size.height)
            }
        });

        if (animated) {
            if (![self isPresented]) {
                if ([self animation] == kLPopupAnimationSlide) {
                    [self setAlpha:0.];

                    CGRect startFrame = finalFrame;
                    startFrame.origin.y += 10.;

                    [self setFrame:startFrame];
                } else if ([self animation] == kLPopupAnimationPop) {
                    [self setFrame:finalFrame];
                    [self setAlpha:.5];
                    [self setTransform:CGAffineTransformMakeScale(.75, .75)];

                    [UIView animateWithDuration:.15 animations:^{
                        [self setTransform:CGAffineTransformMakeScale(1.1, 1.1)];
                        [self setAlpha:1.];
                    } completion:^(BOOL finished) {
                        [UIView animateWithDuration:.1 animations:^{
                            [self setTransform:CGAffineTransformIdentity];
                        }];
                    }];
                }
                
                [self redrawIfNeeded];
                
                if ([self animation] == kLPopupAnimationSlide) {
                    [UIView animateWithDuration:.3 animations:^{
                        [self setAlpha:1.];
                        [self setFrame:finalFrame];
                    }];
                }
            } else {
                [self redrawIfNeeded];
                
                [UIView animateWithDuration:.3 animations:^{
                    [self setFrame:finalFrame];
                }];
            }
        } else {
            [self setAlpha:1.];
            [self setFrame:finalFrame];
            
            [self redrawIfNeeded];
        }
    }
}

- (void)dismissAnimated:(BOOL)animated
{
    if ([self isPresented]) {
        if (animated) {
            CGRect frame = [self frame];
            frame.origin.y += 10.;

            [UIView animateWithDuration:.3 animations:^{
                [self setAlpha:0.];
                [self setFrame:frame];
            } completion:^(BOOL finished) {
                [self finalizeDismiss];
            }];
        } else {
            [self finalizeDismiss];
        }
    }
}

- (void)autoDismissAnimated:(BOOL)animated atTimeInterval:(NSTimeInterval)timeInvertal
{
    if (![self autoDismissTimer]) {
        $weakify(self) {
            [self setAutoDismissTimer:
             [LTimer timerWithTimeInterval:timeInvertal block:^{
                $strongify(self) {
                    [self dismissAnimated:animated];
                    [self notifyWasDismissedByUser];
                }
            }]];
        }
    }

    [self.autoDismissTimer start];
}

- (void)dismissByUser
{
    [self setShouldHighlight:YES];

    [self setNeedsDisplay];
    
    [self dismissAnimated:YES];
    
    [self notifyWasDismissedByUser];
}


#pragma mark - Private Methods
//*********************************************************************************************************************//

- (void)redrawIfNeeded
{
    CGRect redrawRect = CGRectNull;

    if (!CGSizeEqualToSize([self previousFrameSize], [self bounds].size)) {
        [self setPreviousFrameSize:self.bounds.size];

        redrawRect = [self bounds];

        if ([self pointDirection] == kLPopupPointDirectionUp) {
            redrawRect.origin.y += [self pointerSize];
        }

        redrawRect.size.height  -= [self pointerSize];
    }

    if (!CGPointEqualToPoint([self previousTargetPoint], [self targetPoint])) {
        [self setPreviousTargetPoint:self.targetPoint];

        CGRect pointerRect = [self bounds];

        if ([self pointDirection] != kLPopupPointDirectionUp) {
            pointerRect.origin.y    = CGRectGetHeight([self bounds]) - [self pointerSize] - [self cornerRadius];
        }

        pointerRect.size.height     = [self pointerSize] + [self cornerRadius];

        redrawRect = CGRectUnion(redrawRect, pointerRect);
    }

    if (!CGRectIsEmpty(redrawRect)) {
        [self setNeedsDisplayInRect:redrawRect];
    }
}

- (void)updateBubbleSize
{
    CGSize bubbleSize = [self.customView frame].size;

    [self setBubbleSize:((CGSize) {
        .width  = bubbleSize.width  + self.cornerRadius *2.,
        .height = bubbleSize.height + self.cornerRadius *2.
    })];
}

- (void)updateText
{
    if ([self text]) {
        if (![self.customView isKindOfClass:[UILabel class]]) {
            [self setCustomView:[UILabel new]];
        }

        id textLabel = [self customView];
        [textLabel setUserInteractionEnabled:NO];
        [textLabel setBackgroundColor:self.backgroundColor];
        [textLabel setLineBreakMode:NSLineBreakByCharWrapping];

        [textLabel setText:self.text];
        [textLabel setFont:self.textFont];
        [textLabel setTextColor:self.textColor];
        [textLabel setTextAlignment:self.textAlignment];
        [textLabel setBackgroundColor:self.backgroundColor];

        CGRect textFrame    = {
            .origin = {0},
            .size   = [textLabel sizeThatFits:$screensize]
        };

        [textLabel setFrame:textFrame];
    }
}

- (void)updatePresentedView
{
    if ([self customView]) {
        if ([self presentedView] != [self customView]) {
            [self.presentedView removeFromSuperview];
            [self addSubview:self.customView];
            [self setPresentedView:self.customView];
        }

        CGRect customViewFrame    = {
            .origin = {
                .x = self.cornerRadius + (self.sidePadding),
                .y = self.cornerRadius + (self.pointDirection == kLPopupPointDirectionUp? self.pointerSize: 0.)
            },
            .size   = [self customView].frame.size
        };

        [self.customView setFrame:customViewFrame];
    }
}

- (void)notifyWasDismissedByUser
{
    if ([self userDismissVerb]) {
        [self userDismissVerb]();
    }
}

- (void)finalizeDismiss
{
    [self setPresented:NO];

    [self.autoDismissTimer stop];
    [self setAutoDismissTimer:nil];

    if ([self dismissTarget]) {
        [self.dismissTarget removeFromSuperview];
        [self setDismissTarget:nil];
    }

    [self removeFromSuperview];

    [self setShouldHighlight:NO];

    [self setTargetView:nil];
}

@end
