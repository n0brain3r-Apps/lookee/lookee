//
//  LMenuPopover.h
//  Writeability
//
//  Created by Jelo Agnasin on 10/17/13.
//  Copyright (c) 2013 CNL. All rights reserved.
//

#import <UIKit/UIKit.h>


extern const CGFloat LMenuPopoverWidth;
extern const CGFloat LMenuPopoverButtonHeight;


#pragma mark - LMenuButton Interface
//*********************************************************************************************************************//

@interface LMenuButton : UIButton

@end


#pragma mark - LMenuPopover Interface
//*********************************************************************************************************************//

typedef NS_ENUM(NSInteger, LMenuPopoverType) {
    LMenuPopoverTypeDark,
    LMenuPopoverTypeLight,
};

@interface LMenuPopover : UIView

@property (nonatomic, readwrite, weak) NSArray *buttons;

- (instancetype)initWithFrame:(CGRect)frame type:(LMenuPopoverType)type;

@end