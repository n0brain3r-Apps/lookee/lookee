//
//  LSearchBar.m
//  Writeability
//
//  Created by Jelo Agnasin on 10/15/13.
//  Copyright (c) 2013 CNL. All rights reserved.
//

#import "LSearchBar.h"

#import "LConstants.h"

//*********************************************************************************************************************//
#pragma mark -
#pragma mark LSearchBar
#pragma mark -
//*********************************************************************************************************************//

@implementation LSearchBar

- (instancetype)init
{
    self = [super init];
    if (!self) return nil;
    
    self.backgroundImage = [UIImage new];
    
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (!self) return nil;
    
    self.backgroundImage = [UIImage new];
    
    return self;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    for (UIView *view in self.subviews) {
        if ([view isKindOfClass:[UITextField class]]) {
            UITextField *textField = (UITextField *)view;
            textField.font = [[UIFont LDefaultFont] fontWithSize:textField.font.pointSize];
            for (UIView *subview in textField.subviews) {
                if ([subview isKindOfClass:NSClassFromString(@"UITextFieldBorderView")]) {
                    subview.hidden = YES;
                }
            }
        }
    }
}

@end