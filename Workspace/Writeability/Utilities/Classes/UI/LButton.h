//
//  LToolbarButton.h
//  Writeability
//
//  Created by Jelo Agnasin on 10/10/13.
//  Copyright (c) 2013 CNL. All rights reserved.
//


#import <UIKit/UIKit.h>



@class LPopoverController;



@interface LButton : UIButton

+ (LButton *)button;
+ (LButton *)buttonWithFrame:(CGRect)frame;

- (void)setNormalImage:(UIImage *)image;
- (void)setSelectedImage:(UIImage *)image;
- (void)setBackgroundImage:(UIImage *)image;

@end
