//
//  LMenuPopover.m
//  Writeability
//
//  Created by Jelo Agnasin on 10/17/13.
//  Copyright (c) 2013 CNL. All rights reserved.
//

#import "LMenuPopover.h"

#import "LConstants.h"

// custom views
#import "LLineLayer.h"

// categories
#import <Utilities/UIColor+Utilities.h>

//*********************************************************************************************************************//
#pragma mark -
#pragma mark LMenuPopover
#pragma mark -
//*********************************************************************************************************************//

@interface LMenuPopover ()

@property (nonatomic, readwrite, assign) LMenuPopoverType type;
@property (nonatomic, readwrite, strong) NSMutableArray *lineLayers;

@end

@implementation LMenuPopover


#pragma mark - Static Constants
//*********************************************************************************************************************//

const CGFloat LMenuPopoverWidth = 218.0;
const CGFloat LMenuPopoverButtonHeight = 46.0;

static NSString *const kBackgroundColor = @"262626";


#pragma mark - Memory Management
//*********************************************************************************************************************//

- (void)initCode
{
    if (self.type == LMenuPopoverTypeDark) {
        self.backgroundColor = [UIColor colorFromHexString:kBackgroundColor];
    } else if (self.type == LMenuPopoverTypeLight) {
        self.backgroundColor = [UIColor whiteColor];
    }
    self.lineLayers = [NSMutableArray array];
}

- (instancetype)initWithFrame:(CGRect)frame type:(LMenuPopoverType)type
{
    self = [super initWithFrame:frame];
    if (!self) return nil;
    
    self.type = type;
    [self initCode];
    
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (!self) return nil;
    
    self.type = LMenuPopoverTypeDark;
    [self initCode];
    
    return self;
}


#pragma mark - Properties
//*********************************************************************************************************************//

- (void)setButtons:(NSArray *)buttons
{
    [_buttons makeObjectsPerformSelector:@selector(removeFromSuperview)];
    _buttons = buttons;
    
    for (UIButton *button in _buttons) {
        [self addSubview:button];
    }
    
    while (_buttons.count > self.lineLayers.count) {
        LLineLayer *layer = [LLineLayer new];
        [self.layer addSublayer:layer];
        [self.lineLayers addObject:layer];
    }
    
    while (self.lineLayers.count > _buttons.count) {
        [self.lineLayers.lastObject removeFromSuperlayer];
        [self.lineLayers removeLastObject];
    }
    
    [self setNeedsLayout];
}


#pragma mark - Layout Subviews
//*********************************************************************************************************************//

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    CGRect frame;
    
    frame = CGRectZero;
    frame.size.width = LMenuPopoverWidth;
    frame.size.height = LMenuPopoverButtonHeight;

    NSInteger count = self.buttons.count;
    for (NSInteger x = 0; x < count; x++) {
        UIButton *button    = self.buttons[x];
        LLineLayer *layer   = self.lineLayers[x];
        
        button.frame = frame;
        
        CGRect copy = frame;
        copy.origin.x = LC_DEFAULT_MARGIN;
        copy.origin.y += (copy.size.height - 1.0);
        copy.size.width -= 2 * LC_DEFAULT_MARGIN;
        layer.frame = copy;
        
        if (self.type == LMenuPopoverTypeDark) {
            [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            layer.backgroundColor = [UIColor blackColor].CGColor;
        } else if (self.type == LMenuPopoverTypeLight) {
            [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            layer.backgroundColor = [UIColor LBorderColor].CGColor;
        }
        
        
        frame.origin.y += frame.size.height;
    }
}


@end


//*********************************************************************************************************************//
#pragma mark -
#pragma mark LMenuButton
#pragma mark -
//*********************************************************************************************************************//

@implementation LMenuButton


#pragma mark - Static Constants
//*********************************************************************************************************************//

static const CGFloat kMenuButtonFontSize = 14.0;


#pragma mark - Memory Management
//*********************************************************************************************************************//

- (instancetype)init
{
    self = [super init];
    if (!self) return nil;
    
    self.titleLabel.font = [[UIFont LDefaultFont] fontWithSize:kMenuButtonFontSize];
    
    return self;
}

@end