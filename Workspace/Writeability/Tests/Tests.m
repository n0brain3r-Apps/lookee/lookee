//
//  LookeeTests.m
//  LookeeTests
//
//  Created by Ryan Blonna on 16/1/15.
//  Copyright (c) 2015 CNL. All rights reserved.
//


#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>

#import <Utilities/LSerializable.h>
#import <Utilities/NSObject+Utilities.h>

#import "LGenericTestObject.h"




//*********************************************************************************************************************//
#pragma mark -
#pragma mark Private Classes
#pragma mark -
//*********************************************************************************************************************//




@interface LGenericTestObject (Serializable) <LSerializable>

@end




//*********************************************************************************************************************//
#pragma mark -
#pragma mark LookeeTests
#pragma mark -
//*********************************************************************************************************************//




@interface Tests : XCTestCase

@end

@implementation Tests

- (void)setUp {
    [super setUp];
    
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.

    [super tearDown];
}


#pragma mark - Test Methods
//*********************************************************************************************************************//

- (void)test
{
    id testObject = [LGenericTestObject new];

    id JSONString = [testObject convertToJSONString];
    NSLog(@"Serialized: %@", JSONString);

    id object = [NSObject newFromJSONString:JSONString];
    NSLog(@"Deserialized: %@", [object propertyDump]);

    [[object invocation] setTarget:object];
    [[object invocation] invoke];
}

@end




//*********************************************************************************************************************//
#pragma mark -
#pragma mark Private Implementations
#pragma mark -
//*********************************************************************************************************************//




@implementation LGenericTestObject (Serializable)

@end
