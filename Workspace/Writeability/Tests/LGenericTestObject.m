//
//  LGenericTestObject.m
//  Writeability
//
//  Created by Ryan Blonna on 16/1/15.
//  Copyright (c) 2015 CNL. All rights reserved.
//


#import "LGenericTestObject.h"




//*********************************************************************************************************************//
#pragma mark -
#pragma mark LGenericTestObject
#pragma mark -
//*********************************************************************************************************************//




@implementation LGenericTestObject

#pragma mark - Constructors/Destructors
//*********************************************************************************************************************//

- (instancetype)init
{
    if ((self = [super init])) {
        _type               = [self class];
        _integer            = (3);
        _string             = (@"string");

        _rect               = ((CGRect) {
            .origin = {
                .x = 33.,
                .y = 69.
            },
            .size   = {
                .width  = 128.,
                .height = 256.
            }
        });

        _array              = [@[@"element1", [NSNull null], @3] copy];
        _mutableDictionary  = [@{ @"key1": @"value1", @"key2": [NSDate date], @3: @3 } mutableCopy];
        _data               = [@"data" dataUsingEncoding:NSUTF8StringEncoding];

        SEL                 selector    = @selector(selectorWithArgument1:argument2:argument3:);
        NSMethodSignature   *signature  = [self.class instanceMethodSignatureForSelector:selector];

        _invocation         = [NSInvocation invocationWithMethodSignature:signature];
        _invocation.target   = self;
        _invocation.selector = selector;

        id argument1 = (@3.9);
        id argument2 = [NSSet setWithArray:@[[NSOrderedSet orderedSetWithArray:_array], _array, _mutableDictionary]];
        id argument3 = [UIColor blueColor];

        [_invocation setArgument:&argument1 atIndex:2];
        [_invocation setArgument:&argument2 atIndex:3];
        [_invocation setArgument:&argument3 atIndex:4];

        _nilObject          = nil;
        _readonly           = (@"readonly");
        _view               = [[UIView alloc] initWithFrame:_rect];
    }

    return self;
}


#pragma mark - Private Methods
//*********************************************************************************************************************//

- (void)selectorWithArgument1:(NSNumber *)argument1 argument2:(NSSet *)argument2 argument3:(UIColor *)argument3
{
    NSLog(@"Invoked %@: argument1: %@, argument2: %@, argument3: %@",
          NSStringFromSelector(_cmd), argument1, argument2, argument3);
}

- (void)selectorWithArgument1:(NSNumber *)argument1 argument2:(NSSet *)argument2
{
    NSLog(@"Invoked %@: argument1: %@, argument2: %@",
          NSStringFromSelector(_cmd), argument1, argument2);
}

- (void)selectorWithArgument1:(NSNumber *)argument1
{
    NSLog(@"Invoked %@: argument1: %@",
          NSStringFromSelector(_cmd), argument1);
}

- (void)selectorWithNoArguments
{
    NSLog(@"Invoked %@", NSStringFromSelector(_cmd));
}

@end
