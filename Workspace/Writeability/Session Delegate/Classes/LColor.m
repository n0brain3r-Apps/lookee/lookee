//
//  LColor.m
//  Writeability
//
//  Created by Ryan on 6/29/14.
//  Copyright (c) 2014 CNL. All rights reserved.
//


#import "LColor.h"

#import <Utilities/LMacros.h>

#import <Utilities/UIColor+Utilities.h>




//*********************************************************************************************************************//
#pragma mark -
#pragma mark LColor
#pragma mark -
//*********************************************************************************************************************//




@implementation LColor

#pragma mark - Class Methods
//*********************************************************************************************************************//

+ (instancetype)colorWithObject:(UIColor *)object
{
    return [[self alloc] initWithObject:object];
}


#pragma mark - Constructors/Destructors
//*********************************************************************************************************************//

- (instancetype)initWithObject:(UIColor *)object
{
    if ((self = [super init])) {
        _object = object;
    }

    return self;
}


#pragma mark - Overridden Methods
//*********************************************************************************************************************//

- (NSString *)description
{
    return [NSString stringWithFormat:
            @"<%@: %p, value: %@>",
            $classname,
            self,
            self.object.hexString];
}

@end
