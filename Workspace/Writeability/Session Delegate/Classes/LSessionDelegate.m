//
//  LSessionDelegate.m
//  Writeability
//
//  Created by Ryan on 6/8/14.
//  Copyright (c) 2014 CNL. All rights reserved.
//


#import "LSessionDelegate.h"

#import "LPageTurn.h"
#import "LPageZoom.h"
#import "LTool.h"
#import "LPoint.h"
#import "LText.h"
#import "LTextArea.h"
#import "LTextRange.h"
#import "LAction.h"
#import "LColor.h"
#import "LWeight.h"
#import "LFont.h"

#import "LPage.h"

#import "LSessionViewManager.h"
#import "LSessionController.h"

#import "LDocumentContent.h"

#import "LDocument+Annotator.h"

#import "LPage+UndoManager.h"

#import "DiffMatchPatch.h"

#import <Networking/LNetworking.h>
#import <Networking/LNUser.h>

#import <Annotation/LAnnotator+Text.h>

#import <Utilities/LGeometry.h>

#import <Utilities/NSObject+Utilities.h>
#import <Utilities/NSString+Utilities.h>
#import <Utilities/NSAttributedString+Attributes.h>
#import <Utilities/NSOperationQueue+Utilities.h>
#import <Utilities/UIColor+Utilities.h>





//*********************************************************************************************************************//
#pragma mark -
#pragma mark Private Classes
#pragma mark -
//*********************************************************************************************************************//




@interface LAnnotationProperties : NSObject

@property (nonatomic, readwrite, assign) CGRect             area;

@property (nonatomic, readwrite, assign) CGRect             target;

@property (nonatomic, readwrite, assign) NSRange            range;

@property (nonatomic, readwrite, assign) CGFloat            weight;

@property (nonatomic, readwrite, strong) UIColor            *color;

@property (nonatomic, readwrite, strong) NSString           *fontName;

@property (nonatomic, readwrite, strong) NSAttributedString *text;

@end




//*********************************************************************************************************************//
#pragma mark -
#pragma mark LSessionDelegate
#pragma mark -
//*********************************************************************************************************************//




@interface LSessionDelegate ()

@property (nonatomic, readwrite , weak  ) LDocumentContent      *content;

@property (nonatomic, readwrite , weak  ) LSessionController    *controller;

@property (nonatomic, readonly  , strong) LNSelf                *user;

@property (nonatomic, readwrite , weak  ) LNUser                *viewing;

@property (nonatomic, readonly  , strong) NSMutableArray        *annotationProperties;

@property (nonatomic, readonly  , strong) NSMutableArray        *annotationObjects;

@property (nonatomic, readonly  , strong) NSMutableDictionary   *participants;

@property (nonatomic, readonly  , strong) NSMutableDictionary   *monitoredParticipants;

@property (nonatomic, readwrite , assign) NSUInteger            connectedPageNumber;

@property (nonatomic, readwrite , assign) CGRect                connectedZoomRect;

@property (nonatomic, readonly  , assign) NSUInteger            pageNumber;

@property (nonatomic, readonly  , assign) NSUInteger            pageCount;

@property (nonatomic, readonly  , assign) CGRect                zoomRect;

@property (nonatomic, readwrite , assign) BOOL                  annotationBegan;

@property (nonatomic, readwrite , assign) BOOL                  canNavigate;

@property (nonatomic, readwrite , assign, getter = isSoftLocked     ) BOOL softLocked;

@property (nonatomic, readonly  , assign, getter = isLocked         ) BOOL locked;

@property (nonatomic, readonly  , assign, getter = isWriter         ) BOOL writer;

@property (nonatomic, readonly  , assign, getter = isPresenter      ) BOOL presenter;

@property (nonatomic, readonly  , assign, getter = isAdministrator  ) BOOL administrator;

@end

@implementation LSessionDelegate

#pragma mark - Constructors/Destructors
//*********************************************************************************************************************//

- (instancetype)init
{
    if ((self = [super init])) {
        _annotationProperties   = [NSMutableArray new];
        _annotationObjects      = [NSMutableArray new];

        _participants           = [NSMutableDictionary new];
        _monitoredParticipants  = [NSMutableDictionary new];

        [self startMonitoringParticipants];
    }

    return self;
}

- (void)dealloc
{
    DBGMessage(@"deallocated %@", self);
}


#pragma mark - Properties
//*********************************************************************************************************************//

@dynamic pageNumber;

- (NSUInteger)pageNumber
{
    return [self.content pageNumber];
}

@dynamic pageCount;

- (NSUInteger)pageCount
{
    return [self.content pageCount];
}

@dynamic zoomRect;

- (CGRect)zoomRect
{
    return [self.content zoomRect];
}

@dynamic canNavigate;

- (BOOL)canNavigate
{
    return ![self.controller isLocked];
}

- (void)setCanNavigate:(BOOL)canNavigate
{
    [self.controller setLocked:!canNavigate];
}

@dynamic softLocked;

- (BOOL)isSoftLocked
{
    return [self.controller isSoftLocked];
}

- (void)setSoftLocked:(BOOL)softLocked
{
    [self.controller setSoftLocked:softLocked];
}

@dynamic locked;

- (BOOL)isLocked
{
    return [self.user isLocked];
}

@dynamic writer;

- (BOOL)isWriter
{
    return (self.user.permissions & kLNUserPermissionsWrite) || [self isPresenter];
}

@dynamic presenter;

- (BOOL)isPresenter
{
    return  (self.user.permissions & kLNUserPermissionsPresent) || [self isAdministrator];
}

@dynamic administrator;

- (BOOL)isAdministrator
{
    return (self.user.permissions & kLNUserPermissionsAdmin);
}

@dynamic user;

- (LNSelf *)user
{
    return [LNetworking session].me;
}


#pragma mark - Private Methods
//*********************************************************************************************************************//

- (LPage *)pageAtLocation:(NSUInteger)location
{
    return [self.content.document pageAtLocation:location];
}

- (void)startMonitoringController
{
    $weakify(self) {
        [self.controller observerForKeyPath:@Keypath(self.controller.softLocked)] (^{
            $strongify(self) {
                if ([self isSoftLocked]) {
                    [self synchronizePageNumberWithCompletion:^{
                        [self synchronizeZoomRect];
                    }];
                }
            }
        });
    }
}

- (void)startMonitoringDocument
{
    for (NSUInteger location = 1, pageCount = [self pageCount]; location <= pageCount; location ++) {
        LPage *page = [self pageAtLocation:location];
        // TODO: observing currentAnnotator is dangerous; setting annotatorIndex changes its value
        $weakify(self, page) {
            [page observerForKeyPath:@"currentAnnotator.annotationTool"] (^{
                $strongify(self, page) {
                    if ([page annotatorIndex] == 0) {
                        [self updateAnnotationToolForPageAtLocation:location];
                        [self updateAnnotationColorForPageAtLocation:location];
                        [self updateAnnotationWeightForPageAtLocation:location];
                    }
                }
            });

            [page observerForKeyPath:@"currentAnnotator.annotationTool.properties"] (^{
                $strongify(self, page) {
                    if ([page annotatorIndex] == 0) {
                        [self updateAnnotationColorForPageAtLocation:location];
                        [self updateAnnotationWeightForPageAtLocation:location];
                    }
                }
            });

            [page observerForKeyPath:@"currentAnnotator.annotationTool.properties.color"] (^{
                $strongify(self, page) {
                    if ([page annotatorIndex] == 0) {
                        [self updateAnnotationColorForPageAtLocation:location];
                    }
                }
            });

            [page observerForKeyPath:@"currentAnnotator.annotationTool.properties.weight"] (^{
                $strongify(self, page) {
                    if ([page annotatorIndex] == 0) {
                        [self updateAnnotationWeightForPageAtLocation:location];
                    }
                }
            });
        }
    }
}

- (void)startMonitoringContent
{
    $weakify(self) {
        [self.content observerForKeyPath:@Keypath(self.content.pageNumber)] (^(LKVONotification *notification) {
            $strongify(self) {
                [self updatePageNumberFromPageAtLocation:[notification.oldValue integerValue]];
            }
        });

        [self.content observerForKeyPath:@Keypath(self.content.zoomRect)] (^{
            $strongify(self) {
                [self updatePageZoomForPageAtLocation:self.pageNumber];
            }
        });
    }
}

- (void)startMonitoringParticipants
{
    $weakify(self) {
        [self observerForNotificationKey:kLNetworkingSessionWasJoinedNotification](^(NSNotification *notification) {
            LNUser *user = [[notification object] me];

            [user observerForKeyPath:@Keypath(user.permissions)] (^{
                $strongify(self) {
                    [$mainqueue addSynchronousOperationWithBlock:^{
                        if ([self isWriter]) {
                            NSUInteger location = 1;

                            for (NSMutableArray *pageObject in [self annotationObjects]) {
                                for (id object in pageObject) {
                                    [self.user.book[location -1] addObject:object];
                                }

                                [pageObject removeAllObjects];
                                location ++;
                            }
                        }
                    }];
                }
            });

            [user observerForKeyPath:@Keypath(user.locked)] (^{
                $strongify(self) {
                    [$mainqueue addSynchronousOperationWithBlock:^{
                        if ([self isLocked]) {
                            [self synchronizePageNumberWithCompletion:nil];
                        }

                        [self setCanNavigate:![self isLocked]];
                    }];
                }
            });
        });

        [self.annotationProperties addObject:[LAnnotationProperties new]];

        [self observerForNotificationKey:kLNSessionJoinedUserNotification]
        (^(NSNotification *notification) {
            $strongify(self) {
                NSUInteger  index   = [self.participants count]+1;
                LNUser      *user   = [notification object];

                [self.annotationProperties addObject:[LAnnotationProperties new]];
                [self.content.document addAnnotators:1 hidden:![user isHost]];

                [self participants][user.ID] = @(index);

                $weakify(user) {
                    for (NSUInteger location = 1; location <= [self pageCount]; location ++) {
                        [[self pageAtLocation:location] setTag:^{
                            $safify(user) {
                                return [user name];
                            }
                        } forAnnotatorAtIndex:index];
                    }
                }

                if ([user isHost]) {
                    [self synchronizeSessionWithParticipant:user];
                    [self startMonitoringParticipant:user];
                }
            }
        });
    }
}

- (void)startMonitoringParticipant:(LNUser *)participant
{
    NSUInteger pageCount = [self pageCount];

    for (NSUInteger location = 1; location <= pageCount; location ++) {
        [self startMonitoringParticipant:participant pageAtLocation:location];
    }

    $weakify(self, participant) {
        [participant initializedObserverForKeyPath:@Keypath(participant.book)](^{
            $strongify(participant) {
                if (![participant book]) { return; }

                [participant.book observerForKeyPath:@Keypath(participant.book.count)](^{
                    $strongify(self, participant) {
                        [self startMonitoringParticipant:participant pageAtLocation:participant.book.count];
                    }
                });

                [participant.book synchronize];
            }
        });
    }
}

- (void)startMonitoringParticipant:(LNUser *)participant pageAtLocation:(NSUInteger)location
{
    if (location <= [participant.book count]) {
        LNList *objects = participant.book[location-1];

        $weakify(self, participant, objects) {
            [objects observerForKeyPath:@Keypath(objects.count)](^{
                $strongify(self, participant, objects) {
                    [$mainqueue addSynchronousOperationWithBlock:^{
                        [self
                         renderPageObject   :objects.lastObject
                         forParticipant     :participant
                         toPageAtLocation   :location];
                    }];
                }
            });
        }

        [objects synchronize];
    }
}

- (void)stopMonitoringParticipant:(LNUser *)participant
{
    [participant.book destroyObserversForKeyPath:@Keypath(participant.book.count)];

    for (LNList *objects in participant.book) {
        [objects destroyObserversForKeyPath:@Keypath(objects.count)];
    }
}

- (void)synchronizeSessionWithParticipant:(LNUser *)participant
{
    NSUInteger location = 1;

    for (LNList *pageObjects in [participant book]) {
        [self clearAllPageObjectsForParticipant:participant fromPageAtLocation:location];

        for (id pageObject in pageObjects) {
            [self renderPageObject:pageObject forParticipant:participant toPageAtLocation:location];
        }

        location ++;
    }
}

- (void)synchronizePageNumberWithCompletion:(void(^)(void))completion
{
    NSUInteger pageNumber = [self connectedPageNumber]?:1;

    if ([self.content pageNumber] != pageNumber) {
        [self.content setPageNumber:pageNumber animated:YES completion:completion];
    }
}

- (void)synchronizeZoomRect
{
    if (!CGRectIsEmpty([self connectedZoomRect])) {
        [self.content setZoomRect:self.connectedZoomRect animated:YES];
    }
}

- (void)sendToolSettingsForCurrentPage
{
    [self sendToolSettingsForPageAtLocation:self.pageNumber];
}

- (void)sendToolSettingsForPageAtLocation:(NSUInteger)location
{
    [self updateAnnotationToolForPageAtLocation:location];
    [self updateAnnotationColorForPageAtLocation:location];
    [self updateAnnotationWeightForPageAtLocation:location];
    [self updateAnnotationFontForPageAtLocation:location];
}

- (void)insertPageObjectListAtLocation:(NSUInteger)location
{
    if (--location < [self pageCount]) {
        [self.annotationObjects addObject:[NSMutableArray new]];
        [self.user.book insertObject:[LNList new] atIndex:location];
    }
}

- (void)addAnnotationObject:(id)object toPageAtLocation:(NSUInteger)location
{
    if ([self isWriter]) {
        [self.user.book[ --location ] addObject:object];
    } else {
        [self.annotationObjects[ --location ] addObject:object];
    }
}

- (void)updateAnnotationColorForPageAtLocation:(NSUInteger)location
{
    if (--location < [self pageCount]) {
        LPage   *page   = [self pageAtLocation:++location];

        LColor  *color  = [LColor colorWithObject:page.currentAnnotator.annotationTool.properties.colorWithAlpha];

        [self addAnnotationObject:color toPageAtLocation:location];
    }
}

- (void)updateAnnotationWeightForPageAtLocation:(NSUInteger)location
{
    if (--location < [self pageCount]) {
        LPage   *page   = [self pageAtLocation:++location];

        LWeight *weight = [LWeight weightWithValue:page.currentAnnotator.annotationTool.properties.scaledWeight];

        [self addAnnotationObject:weight toPageAtLocation:location];
    }
}

- (void)updateAnnotationFontForPageAtLocation:(NSUInteger)location
{
    if (--location < [self pageCount]) {
        LAnnotationProperties   *properties = [self annotationProperties][0];

        LFont                   *font       = [LFont fontWithName:properties.fontName];

        [self addAnnotationObject:font toPageAtLocation:location];
    }
}

- (void)updateAnnotationToolForPageAtLocation:(NSUInteger)location
{
    if (--location < [self pageCount]) {
        LPage *page = [self pageAtLocation:++location];

        LTool *tool = [LTool toolWithType:page.currentAnnotator.annotationTool.class];

        [self addAnnotationObject:tool toPageAtLocation:location];
    }
}

- (void)updatePageZoomForPageAtLocation:(NSUInteger)location
{
    if ([self isPresenter]) {
        if (--location < [self pageCount]) {
            LPageZoom *pageZoom = [LPageZoom pageZoomToRect:self.zoomRect];

            [self addAnnotationObject:pageZoom toPageAtLocation:++location];
        }
    }
}

- (void)updatePageNumberFromPageAtLocation:(NSUInteger)location
{
    if ([self isPresenter]) {
        if (--location < [self pageCount]) {
            LPageTurn *pageTurn = [LPageTurn pageTurnToPageNumber:self.pageNumber];

            [self addAnnotationObject:pageTurn toPageAtLocation:++location];
        }
    }
}

- (void)renderPageObject:(id)object forParticipant:(LNUser *)participant toPageAtLocation:(NSUInteger)location
{
    if (object && participant && location) {
        LPage *page = [self pageAtLocation:location];

        NSUInteger index = [page annotatorIndex];

        [page setAnnotatorIndex:[self.participants[participant.ID] integerValue]];

        if ([object isKindOfClass:[LPageTurn class]]) {
            [self renderPageTurn:(id)object fromPageAtLocation:location];
        } else if ([object isKindOfClass:[LPageZoom class]]) {
            [self renderPageZoom:(id)object toPageAtLocation:location];
        } else if ([object isKindOfClass:[LTool class]]) {
            [self renderAnnotationTool:(id)object toPageAtLocation:location];
        } else if ([object isKindOfClass:[LColor class]]) {
            [self renderAnnotationColor:(id)object toPageAtLocation:location];
        } else if ([object isKindOfClass:[LWeight class]]) {
            [self renderAnnotationWeight:(id)object toPageAtLocation:location];
        } else if ([object isKindOfClass:[LFont class]]) {
            [self renderAnnotationFont:(id)object toPageAtLocation:location];
        } else if ([object isKindOfClass:[LAction class]]) {
            [self renderAnnotationAction:(id)object toPageAtLocation:location];
        } else if ([object isKindOfClass:[LPoint class]]) {
            [self renderAnnotationPoint:(id)object toPageAtLocation:location];
        } else if ([object isKindOfClass:[LArea class]]) {
            [self renderAnnotationArea:(id)object toPageAtLocation:location];
        } else if ([object isKindOfClass:[LTextRange class]]) {
            [self renderAnnotationTextRange:(id)object toPageAtLocation:location];
        } else if ([object isKindOfClass:[LText class]]) {
            [self renderAnnotationText:(id)object toPageAtLocation:location];
        }

        [page setAnnotatorIndex:index];
    }
}

- (void)clearAllPageObjectsForParticipant:(LNUser *)participant fromPageAtLocation:(NSUInteger)location
{
    LPage *page = [self pageAtLocation:location];

    NSUInteger index = [page annotatorIndex];

    [page setAnnotatorIndex:[self.participants[participant.ID] integerValue]];

    [page.currentAnnotator removeAllAnnotations];

    [page setAnnotatorIndex:index];
}

- (void)renderPageTurn:(LPageTurn *)pageTurn fromPageAtLocation:(NSUInteger)location
{
    [self setConnectedPageNumber:[pageTurn pageNumber]];

    if ([self isSoftLocked]) {
        [self.content setPageNumber:[pageTurn pageNumber] animated:YES];
    }
}

- (void)renderPageZoom:(LPageZoom *)pageZoom toPageAtLocation:(NSUInteger)location
{
    [self setConnectedZoomRect:[pageZoom rect]];

    if ([self isSoftLocked] && [self pageNumber] == location) {
        [self.content setZoomRect:[pageZoom rect] animated:YES];
    }
}

- (void)renderAnnotationTool:(LTool *)tool toPageAtLocation:(NSUInteger)location
{
    LPage *page = [self pageAtLocation:location];

    page.currentAnnotator.annotationTool = [LATool newForType:tool.type];
}

- (void)renderAnnotationColor:(LColor *)color toPageAtLocation:(NSUInteger)location
{
    LPage *page = [self pageAtLocation:location];

    [page.currentAnnotator.annotationTool.properties setColor:color.object];
    [page.currentAnnotator.annotationTool.properties setAlpha:color.object.alpha];

    [self.annotationProperties[page.annotatorIndex] setColor:color.object];
}

- (void)renderAnnotationWeight:(LWeight *)weight toPageAtLocation:(NSUInteger)location
{
    LPage *page = [self pageAtLocation:location];

    [page.currentAnnotator.annotationTool.properties setWeight:weight.value];

    [(LAProperties *)self.annotationProperties[page.annotatorIndex] setWeight:weight.value];
}

- (void)renderAnnotationFont:(LFont *)font toPageAtLocation:(NSUInteger)location
{
    LPage *page = [self pageAtLocation:location];

    [self.annotationProperties[page.annotatorIndex] setFontName:font.name];
}

- (void)renderAnnotationAction:(LAction *)action toPageAtLocation:(NSUInteger)location
{
    LPage *page = [self pageAtLocation:location];

    if ([action.name isEqualToString:@"undo"]) {
        [page undo];
    } else if ([action.name isEqualToString:@"redo"]) {
        [page redo];
    }
}

- (void)renderAnnotationPoint:(LPoint *)point toPageAtLocation:(NSUInteger)location
{
    LPage   *page       = [self pageAtLocation:location];

    BOOL    isEditor    = [page.currentAnnotator.annotationTool isEditor];

    if ([point isKindOfClass:[LStartPoint class]]) {
        [page.currentAnnotator.annotationTool beginAtPoint:point.coordinates];
    } else if ([point isKindOfClass:[LEndPoint class]]) {
        [page.currentAnnotator.annotationTool endAtPoint:point.coordinates];

        if (!isEditor) {
            [page.recordUndoOperation removeLastAnnotation];
        }
    } else if ([point isKindOfClass:[LPoint class]]) {
        [page.currentAnnotator.annotationTool moveToPoint:point.coordinates];
    }

    if (isEditor) {
        [page removeAnnotationAtIndex:[page.currentAnnotator annotationIndexForPoint:point.coordinates]];
    }
}

- (void)renderAnnotationArea:(LArea *)area toPageAtLocation:(NSUInteger)location
{
    LPage                   *page       = [self pageAtLocation:location];

    LAnnotationProperties   *properties = [self annotationProperties][page.annotatorIndex];

    if ([area isKindOfClass:[LTextArea class]]) {
        [properties setArea:[area rect]];
        [properties setTarget:[area rect]];
        [properties setRange:((NSRange){0})];
        [properties setText:[NSAttributedString new]];
    }

    [properties setTarget:[area rect]];}

- (void)renderAnnotationTextRange:(LTextRange *)range toPageAtLocation:(NSUInteger)location
{
    LPage                   *page       = [self pageAtLocation:location];

    LAnnotationProperties   *properties = [self annotationProperties][page.annotatorIndex];

    NSRange                 bounds      = [range bounds];

    if ((bounds.length > 0) || (bounds.location == NSNotFound)) {
        [page.currentAnnotator removeAnnotationAtIndex:page.currentAnnotator.annotationCount-1];

        id annotation = [properties.text mutableCopy];

        if (bounds.location != NSNotFound) {
            [annotation replaceCharactersInRange:bounds withAttributedString:[NSAttributedString new]];
            [properties setText:[annotation copy]];

            bounds = ((NSRange){0, bounds.location});
        } else {
            [properties setArea:properties.target];
        }

        [page.currentAnnotator setText:annotation inRect:properties.area];
    }

    if (bounds.location != NSNotFound) {
        [properties setRange:bounds];
    }
}

- (void)renderAnnotationText:(LText *)text toPageAtLocation:(NSUInteger)location
{
    LPage                   *page       = [self pageAtLocation:location];

    LAnnotationProperties   *properties = [self annotationProperties][page.annotatorIndex];

    if ([properties.text length] || !CGRectEqualToRect([properties area], [properties target])) {
        [page.currentAnnotator removeAnnotationAtIndex:page.currentAnnotator.annotationCount-1];
    }

    [properties setArea:properties.target];

    id annotation   = [properties.text mutableCopy];

    id attributes
    =
    (@{ NSForegroundColorAttributeName   :
            [properties color],
        NSFontAttributeName              :
            [UIFont fontWithName:[properties fontName]
                            size:[properties weight]]
        });

    id string       = [[NSAttributedString alloc] initWithString:[text string] attributes:attributes];

    NSRange range   = [properties range];

    [annotation insertAttributedString:string atIndex:range.location + range.length];

    range.length   += [string length];

    [properties setRange:range];
    [properties setText:[annotation copy]];

    [page.currentAnnotator setText:annotation inRect:properties.area];
}


#pragma mark - LSessionDelegate
//*********************************************************************************************************************//

- (void)sessionController:(LSessionController *)controller willBeginSessionWithContent:(LDocumentContent *)content
{
    [self setController:controller];
    [self setContent:content];

    for (NSUInteger location = 1, pageCount = [self pageCount]; location <= pageCount; location ++) {
        [self insertPageObjectListAtLocation:location];
    }
}

- (void)sessionController:(LSessionController *)controller didBeginSessionWithContent:(LDocumentContent *)content
{
    [self startMonitoringController];
    [self startMonitoringContent];
    [self startMonitoringDocument];

    if (![self.user isHost]) {
        [self setSoftLocked:YES];
    }

    [self.content setToDefaultState];
}

- (void)sessionController:(LSessionController *)controller toggleLockParticipantWithID:(NSString *)ID
{
    for (LNUser *participant in [[LNetworking session] users]) {
        if ([participant.ID isEqualToString:ID]) {
            [participant setLocked:![participant isLocked]];
            break;
        }
    }
}

- (void)sessionController:(LSessionController *)controller toggleViewParticipantWithID:(NSString *)ID
{
    [self.content.document hideAllAnnotators];

    if ([self.viewing.ID isEqualToString:ID]) {
        [self.viewing setPermissions:kLNUserPermissionsRead];
        LNetworkingUnlinkUserFromCurrentSession(self.viewing);
        [self stopMonitoringParticipant:self.viewing];
        [self setViewing:nil];

        [self setSoftLocked:NO];
        [self.content.document showAnnotatorAtIndex:0];
    } else {
        NSUInteger index = [[self participants][ID] integerValue];

        for (LNUser *participant in [LNetworking session].users) {
            if ([participant.ID isEqualToString:ID]) {
                [self setViewing:participant];
                LNetworkingLinkUserToCurrentSession(participant);
                [participant setPermissions:kLNUserPermissionsPresent];
                [self startMonitoringParticipant:participant];
                break;
            }
        }

        [self setSoftLocked:YES];
        [self.content.document showAnnotatorAtIndex:index];
    }
}


#pragma mark - LDocumentContentDelegate
//*********************************************************************************************************************//

- (void)documentContentDidLoad:(LDocumentContent *)content
{

}

- (BOOL)documentContentShouldAllowUserPageTurn:(LDocumentContent *)content
{
    if ([self canNavigate]) {
        if ([self isSoftLocked]) {
            [self setSoftLocked:NO];
        }

        return YES;
    }

    return NO;
}

- (void)documentContent:(LDocumentContent *)content didBeginAnnotationAtPoint:(CGPoint)coordinates
{
    [self setAnnotationBegan:YES];

    [self addAnnotationObject:[LStartPoint pointWithCoordinates:coordinates] toPageAtLocation:[self pageNumber]];
}

- (void)documentContent:(LDocumentContent *)content didMoveAnnotationToPoint:(CGPoint)coordinates
{
    if ([self annotationBegan]) {
        [self addAnnotationObject:[LPoint pointWithCoordinates:coordinates] toPageAtLocation:[self pageNumber]];
    }
}

- (void)documentContent:(LDocumentContent *)content didEndAnnotationAtPoint:(CGPoint)coordinates
{
    if ([self annotationBegan]) {
        [self setAnnotationBegan:NO];

        [self addAnnotationObject:[LEndPoint pointWithCoordinates:coordinates] toPageAtLocation:[self pageNumber]];
    }
}

- (void)documentContent:(LDocumentContent *)content didBeginEditingTextInRect:(CGRect)rect
{
    [self setAnnotationBegan:YES];

    LAnnotationProperties *properties = self.annotationProperties[ 0 ];

    [properties setArea:rect];
    [properties setTarget:rect];
    [properties setRange:((NSRange){0})];
    [properties setText:[NSAttributedString new]];

    [self addAnnotationObject:[LTextArea areaWithRect:rect] toPageAtLocation:[self pageNumber]];}

- (void)documentContent:(LDocumentContent *)content isEditingText:(NSAttributedString *)text inRect:(CGRect)rect
{
    static DiffMatchPatch *diffLibrary;

    if (!diffLibrary) {
        diffLibrary = [DiffMatchPatch new];
    }

    if ([self annotationBegan]) {
        LAnnotationProperties *properties = self.annotationProperties[ 0 ];

        if (!CGRectEqualToRect([properties area], rect)) {
            [properties setArea:rect];
            [properties setTarget:rect];

            [self addAnnotationObject:[LArea areaWithRect:rect] toPageAtLocation:[self pageNumber]];
        }

        NSRange range   = {0};
        NSRange bounds  = [properties range];

        NSArray *diffs = [diffLibrary diff_mainOfOldString:properties.text.string andNewString:text.string];
        
        for (Diff *diff in diffs) {
            switch ([diff operation]) {
                case DIFF_DELETE: {
                    range.length = [diff.text length];

                    [self addAnnotationObject:[LTextRange textRangeWithBounds:range]
                             toPageAtLocation:[self pageNumber]];

                    [properties setRange:((NSRange) {
                        .location   = 0,
                        .length     = range.location
                    })];

                    [properties setText:text];
                } break;
                case DIFF_INSERT: {
                    range.length = [diff.text length];

                    if ((bounds.location + bounds.length) != range.location) {
                        bounds = ((NSRange){range.location, 0});

                        [self addAnnotationObject:[LTextRange textRangeWithBounds:bounds]
                                 toPageAtLocation:[self pageNumber]];
                    }

                    [properties setRange:((NSRange) {
                        .location   = bounds.location,
                        .length     = bounds.length + range.length
                    })];

                    [properties setText:text];

                    [text
                     enumerateAttributesInRange :range
                     options                    :0
                     usingBlock                 :
                     ^(NSDictionary *attributes, NSRange range, BOOL *stop) {
                         NSString   *string = [text.string substringWithRange:range];
                         UIColor    *color  = attributes[NSForegroundColorAttributeName];
                         UIFont     *font   = attributes[NSFontAttributeName];

                         if (![properties.color isEqualToColor:color]) {
                             [properties setColor:color];
                             [self addAnnotationObject:[LColor colorWithObject:color]
                                      toPageAtLocation:[self pageNumber]];
                         }

                         if (![properties.fontName isEqualToString:font.fontName]) {
                             [properties setFontName:font.fontName];
                             [self addAnnotationObject:[LFont fontWithName:font.fontName]
                                      toPageAtLocation:[self pageNumber]];
                         }

                         if (!IsEqual([properties weight], [font pointSize])) {
                             [properties setWeight:font.pointSize];
                             [self addAnnotationObject:[LWeight weightWithValue:font.pointSize]
                                      toPageAtLocation:[self pageNumber]];
                         }

                         [self addAnnotationObject:[LText textWithString:string]
                                  toPageAtLocation:[self pageNumber]];
                     }];
                } break;
                default: {

                } break;
            }

            range.location += [diff.text length];
        }
    }
}

- (void)documentContent:(LDocumentContent *)content didEndEditingTextInRect:(CGRect)rect
{
    if ([self annotationBegan]) {
        [self setAnnotationBegan:NO];

        LAnnotationProperties *properties = self.annotationProperties[ 0 ];

        if (!CGRectEqualToRect([properties area], rect)) {
            [properties setArea:rect];
            [properties setTarget:rect];

            NSRange bounds = {NSNotFound, 0};

            [self addAnnotationObject:[LArea areaWithRect:rect] toPageAtLocation:[self pageNumber]];
            [self addAnnotationObject:[LTextRange textRangeWithBounds:bounds] toPageAtLocation:[self pageNumber]];
        }
    }
}

- (void)documentContent:(LDocumentContent *)content willEditAnnotationAtPoint:(CGPoint)point
{
}

- (void)documentContent:(LDocumentContent *)content didPlaceAnnotationInRect:(CGRect)rect
{
}

- (void)documentContentDidUndo:(LDocumentContent *)content
{
    [self addAnnotationObject:[LAction actionWithName:@"undo"] toPageAtLocation:[self pageNumber]];
}

- (void)documentContentDidRedo:(LDocumentContent *)content
{
    [self addAnnotationObject:[LAction actionWithName:@"redo"] toPageAtLocation:[self pageNumber]];
}

@end




//*********************************************************************************************************************//
#pragma mark -
#pragma mark Private Implementations
#pragma mark -
//*********************************************************************************************************************//




@implementation LAnnotationProperties

@end