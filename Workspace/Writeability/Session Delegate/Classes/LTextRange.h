//
//  LTextIndex.h
//  Writeability
//
//  Created by Ryan on 8/26/14.
//  Copyright (c) 2014 CNL. All rights reserved.
//


#import <Utilities/LSerializable.h>


@interface LTextRange : NSObject <LSerializable>

@property (nonatomic, readwrite, assign) NSRange bounds;


+ (instancetype)textRangeWithBounds:(NSRange)bounds;

@end
