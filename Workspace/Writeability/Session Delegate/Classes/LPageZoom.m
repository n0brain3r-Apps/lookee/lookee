//
//  LPageZoom.m
//  Writeability
//
//  Created by Ryan on 6/28/14.
//  Copyright (c) 2014 CNL. All rights reserved.
//


#import "LPageZoom.h"

#import <Utilities/LMacros.h>




//*********************************************************************************************************************//
#pragma mark -
#pragma mark LPageZoom
#pragma mark -
//*********************************************************************************************************************//




@implementation LPageZoom

#pragma mark - Class Methods
//*********************************************************************************************************************//

+ (instancetype)pageZoomToRect:(CGRect)rect
{
    return [[self alloc] initWithZoomRect:rect];
}


#pragma mark - Constructors/Destructors
//*********************************************************************************************************************//

- (instancetype)initWithZoomRect:(CGRect)rect
{
    if ((self = [super init])) {
        _rect = rect;
    }

    return self;
}


#pragma mark - Overridden Methods
//*********************************************************************************************************************//

- (NSString *)description
{
    return [NSString stringWithFormat:
            @"<%@: %p, rect: %@>",
            $classname,
            self,
            NSStringFromCGRect(self.rect)];
}

@end
