//
//  LPoint.h
//  Writeability
//
//  Created by Ryan on 6/26/14.
//  Copyright (c) 2014 CNL. All rights reserved.
//


#import <UIKit/UIKit.h>

#import <Utilities/LSerializable.h>


@interface LPoint : NSObject <LSerializable>

@property (nonatomic, readwrite, assign) CGPoint coordinates;


+ (instancetype)pointWithCoordinates:(CGPoint)coordinates;

@end

@interface LStartPoint : LPoint

@end

@interface LEndPoint : LPoint

@end
