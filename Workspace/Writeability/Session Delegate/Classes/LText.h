//
//  LText.h
//  Writeability
//
//  Created by Ryan on 8/25/14.
//  Copyright (c) 2014 CNL. All rights reserved.
//


#import <Utilities/LSerializable.h>


@interface LText : NSObject <LSerializable>

@property (nonatomic, readwrite, copy) NSString *string;


+ (instancetype)textWithString:(NSString *)string;

@end
