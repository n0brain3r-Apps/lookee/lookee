//
//  LColor.h
//  Writeability
//
//  Created by Ryan on 6/29/14.
//  Copyright (c) 2014 CNL. All rights reserved.
//


#import <UIKit/UIKit.h>

#import <Utilities/LSerializable.h>


@interface LColor : NSObject <LSerializable>

@property (nonatomic, readwrite, strong) UIColor *object;


+ (instancetype)colorWithObject:(UIColor *)object;

@end
