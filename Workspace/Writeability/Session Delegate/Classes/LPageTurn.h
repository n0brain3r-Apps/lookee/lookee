//
//  LPageTurn.h
//  Writeability
//
//  Created by Ryan on 6/28/14.
//  Copyright (c) 2014 CNL. All rights reserved.
//


#import <Utilities/LSerializable.h>


@interface LPageTurn : NSObject <LSerializable>

@property (nonatomic, readwrite, assign) NSUInteger pageNumber;


+ (instancetype)pageTurnToPageNumber:(NSUInteger)pageNumber;

@end
