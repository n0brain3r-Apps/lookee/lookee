//
//  LWeight.m
//  Writeability
//
//  Created by Ryan on 6/29/14.
//  Copyright (c) 2014 CNL. All rights reserved.
//


#import "LWeight.h"

#import <Utilities/LMacros.h>




//*********************************************************************************************************************//
#pragma mark -
#pragma mark LWeight
#pragma mark -
//*********************************************************************************************************************//




@implementation LWeight

#pragma mark - Class Methods
//*********************************************************************************************************************//

+ (instancetype)weightWithValue:(float)value
{
    return [[self alloc] initWithValue:value];
}


#pragma mark - Constructors/Destructors
//*********************************************************************************************************************//

- (instancetype)initWithValue:(float)value
{
    if ((self = [super init])) {
        _value = value;
    }

    return self;
}


#pragma mark - Overridden Methods
//*********************************************************************************************************************//

- (NSString *)description
{
    return [NSString stringWithFormat:
            @"<%@: %p, value: %f>",
            $classname,
            self,
            self.value];
}

@end
