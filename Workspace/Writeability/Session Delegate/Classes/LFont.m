//
//  LFont.m
//  Writeability
//
//  Created by Ryan on 8/25/14.
//  Copyright (c) 2014 CNL. All rights reserved.
//


#import "LFont.h"

#import <Utilities/LMacros.h>




//*********************************************************************************************************************//
#pragma mark -
#pragma mark LFont
#pragma mark -
//*********************************************************************************************************************//




@implementation LFont

#pragma mark - Class Methods
//*********************************************************************************************************************//

+ (instancetype)fontWithName:(NSString *)name
{
    return [[self alloc] initWithName:name];
}


#pragma mark - Constructors/Destructors
//*********************************************************************************************************************//

- (instancetype)initWithName:(NSString *)name
{
    if ((self = [super init])) {
        _name = [name copy];
    }

    return self;
}


#pragma mark - Overridden Methods
//*********************************************************************************************************************//

- (NSString *)description
{
    return [NSString stringWithFormat:
            @"<%@: %p, name: %@>",
            $classname,
            self,
            self.name];
}

@end
