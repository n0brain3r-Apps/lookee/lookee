//
//  LAction.h
//  Writeability
//
//  Created by Ryan on 6/30/14.
//  Copyright (c) 2014 CNL. All rights reserved.
//


#import <Utilities/LSerializable.h>


@interface LAction : NSObject <LSerializable>

@property (nonatomic, readwrite, strong) NSString *name;


+ (instancetype)actionWithName:(NSString *)name;

@end
