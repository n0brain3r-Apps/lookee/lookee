//
//  LWeight.h
//  Writeability
//
//  Created by Ryan on 6/29/14.
//  Copyright (c) 2014 CNL. All rights reserved.
//


#import <Utilities/LSerializable.h>


@interface LWeight : NSObject <LSerializable>

@property (nonatomic, readwrite, assign) float value;


+ (instancetype)weightWithValue:(float)value;

@end
