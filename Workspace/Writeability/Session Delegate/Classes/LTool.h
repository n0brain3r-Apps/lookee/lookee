//
//  LTool.h
//  Writeability
//
//  Created by Ryan on 6/27/14.
//  Copyright (c) 2014 CNL. All rights reserved.
//


#import <Utilities/LSerializable.h>


@interface LTool : NSObject <LSerializable>

@property (nonatomic, readwrite, strong) Class type;


+ (instancetype)toolWithType:(Class)type;

@end
