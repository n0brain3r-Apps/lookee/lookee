//
//  LTool.m
//  Writeability
//
//  Created by Ryan on 6/27/14.
//  Copyright (c) 2014 CNL. All rights reserved.
//


#import "LTool.h"

#import <Utilities/LMacros.h>




//*********************************************************************************************************************//
#pragma mark -
#pragma mark LTool
#pragma mark -
//*********************************************************************************************************************//




@implementation LTool

#pragma mark - Class Methods
//*********************************************************************************************************************//

+ (instancetype)toolWithType:(Class)class
{
    return [[self alloc] initWithType:class];
}


#pragma mark - Constructors/Destructors
//*********************************************************************************************************************//

- (instancetype)initWithType:(Class)class
{
    if ((self = [super init])) {
        _type = class;
    }

    return self;
}


#pragma mark - Overridden Methods
//*********************************************************************************************************************//

- (NSString *)description
{
    return [NSString stringWithFormat:
            @"<%@: %p, type: %@>",
            $classname,
            self,
            NSStringFromClass(self.type)];
}

@end
