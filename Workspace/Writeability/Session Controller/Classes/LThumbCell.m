//
//  LThumbCell.m
//  Writeability
//
//  Created by Ryan on 6/3/14.
//  Copyright (c) 2014 CNL. All rights reserved.
//


#import "LThumbCell.h"

#import "LConstants.h"




//*********************************************************************************************************************//
#pragma mark -
#pragma mark LThumbCell
#pragma mark -
//*********************************************************************************************************************//




@interface LThumbCell ()

@property (nonatomic, readonly, weak) UIImageView   *thumbView;
@property (nonatomic, readonly, weak) UILabel       *label;
@property (nonatomic, readonly, weak) UILabel       *detail;

@end

@implementation LThumbCell

#pragma mark - Static Objects
//*********************************************************************************************************************//

static CGFloat const kLThumbCellTextMargin = 4.;


#pragma mark - Constructors/Destructors
//*********************************************************************************************************************//

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        [self.contentView setBackgroundColor:[UIColor whiteColor]];

        UIImageView *thumbView  = [UIImageView new];
        UILabel     *label      = [UILabel new];
        UILabel     *detail     = [UILabel new];

        [label setBackgroundColor:[UIColor whiteColor]];
        [label setTextColor:[UIColor LTealColor]];
        [label setFont:[[UIFont LDefaultFont] fontWithSize:14.]];
        [label setTextAlignment:NSTextAlignmentCenter];

        [detail setBackgroundColor:[UIColor whiteColor]];
        [detail setTextColor:[UIColor LTealColor]];
        [detail setFont:[label.font fontWithSize:label.font.pointSize-2.]];
        [detail setTextAlignment:NSTextAlignmentCenter];

        [thumbView setClipsToBounds:YES];
        [thumbView setContentMode:UIViewContentModeScaleAspectFill];

        [self.contentView addSubview:thumbView];
        [self.contentView addSubview:detail];
        [self.contentView addSubview:label];

        _thumbView  = thumbView;
        _label      = label;
        _detail     = detail;
    }
    
    return self;
}


#pragma mark - Overridden Methods
//*********************************************************************************************************************//

- (void)setSelected:(BOOL)selected
{
    [super setSelected:selected];

    if (selected) {
        [UIView animateWithDuration:.15 animations:^{
            self.alpha      = .5;
            self.transform  = CGAffineTransformMakeScale(1.1, 1.1);
        } completion:^(BOOL finished) {
            [UIView animateWithDuration:.15 animations:^{
                self.alpha      = 1.;
                self.transform  = CGAffineTransformIdentity;
            } completion:^(BOOL finished) {
                if ([self selectBlock]) {
                    [self selectBlock]();
                }
            }];
        }];
    }
}

- (void)layoutSubviews
{
    [super layoutSubviews];

    [self.thumbView setFrame:self.bounds];

    CGRect frame        = [self bounds];

    CGRect detailFrame  = [self.detail frame];

    detailFrame = ((CGRect) {
        .origin = {CGRectGetMinX(frame), CGRectGetMaxY(frame) - CGRectGetHeight(detailFrame)},
        .size   = {CGRectGetWidth(detailFrame), CGRectGetHeight(detailFrame)}
    });

    CGRect labelFrame   = [self.label frame];

    labelFrame  = ((CGRect) {
        .origin = {CGRectGetMinX(detailFrame), CGRectGetMinY(detailFrame) - CGRectGetHeight(labelFrame)},
        .size   = {CGRectGetWidth(labelFrame), CGRectGetHeight(labelFrame)}
    });

    [self.label setFrame:labelFrame];
    [self.detail setFrame:detailFrame];
}


#pragma mark - Properties
//*********************************************************************************************************************//

@dynamic thumbnail;

- (UIImage *)thumbnail
{
    return [self.thumbView image];
}

- (void)setThumbnail:(UIImage *)thumbnail
{
    [self.thumbView setImage:thumbnail];
}

@dynamic labelText;

- (NSString *)labelText
{
    return [self.label text];
}

- (void)setLabelText:(NSString *)label
{
    [self.label setText:label];
    [self.label sizeToFit];

    CGRect  frame = CGRectInset([self.label frame], -kLThumbCellTextMargin, -kLThumbCellTextMargin);
    CGFloat limit = [self.contentView bounds].size.width;

    if (isgreater(frame.origin.x + frame.size.width, limit)) {
        frame.size.width = limit -frame.origin.x -kLThumbCellTextMargin;
    }

    [self.label setFrame:frame];

    [self setNeedsLayout];
    [self layoutIfNeeded];
}

@dynamic detailText;

- (NSString *)detailText
{
    return [self.detail text];
}

- (void)setDetailText:(NSString *)detail
{
    [self.detail setText:detail];
    [self.detail sizeToFit];

    CGRect  frame = CGRectInset([self.detail frame], -kLThumbCellTextMargin, -kLThumbCellTextMargin);
    CGFloat limit = [self.contentView bounds].size.width;

    if (isgreater(frame.origin.x + frame.size.width, limit)) {
        frame.size.width = limit -frame.origin.x -kLThumbCellTextMargin;
    }

    [self.detail setFrame:frame];

    [self setNeedsLayout];
    [self layoutIfNeeded];
}

@end
