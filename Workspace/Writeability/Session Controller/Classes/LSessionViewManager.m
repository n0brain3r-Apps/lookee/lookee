//
//  LSessionViewManager.m
//  Writeability
//
//  Created by Ryan on 7/5/14.
//  Copyright (c) 2014 CNL. All rights reserved.
//

#import "LSessionViewManager.h"

#import "LSessionController.h"

#import "LButton.h"

#import "LPopupView.h"

#import "LConstants.h"





//*********************************************************************************************************************//
#pragma mark -
#pragma mark LSessionViewManager
#pragma mark -
//*********************************************************************************************************************//





@interface LSessionViewManager ()

@property (nonatomic, readonly, strong) LPopupView *popup;

@end

@implementation LSessionViewManager

#pragma mark - Class Methods
//*********************************************************************************************************************//

+ (instancetype)manager
{
    static id               instance;
    static dispatch_once_t  token;

    dispatch_once(&token, ^{
        instance = [self new];
    });

    return instance;
}


#pragma mark - Constructors/Destructors
//*********************************************************************************************************************//

- (instancetype)init
{
    if ((self = [super initWithRootViewController:[LSessionController new]])) {
        _popup = [LPopupView new];

        [self.popup setBorderColor:self.popup.backgroundColor];
        [self.popup setHasShadow:YES];
        [self.popup setPointerSize:0.];
    }

    return self;
}


#pragma mark - Overridden Methods
//*********************************************************************************************************************//

- (void)viewDidLoad
{
    [super viewDidLoad];

    [self loadAppearance];
    [self loadNavigationBar];
    [self loadToolbar];
}


#pragma mark - Public Methods
//*********************************************************************************************************************//

- (void)displayNotificationWithMessage:(NSString *)message
{
    UIViewController *controller = [self topViewController];

    [self.popup setText:message];

    [self.popup presentPointingAtView:self.navigationBar inView:controller.view animated:YES];
    [self.popup autoDismissAnimated:YES atTimeInterval:3.];
}


#pragma mark - Private Methods
//*********************************************************************************************************************//

- (void)loadAppearance
{
    [[UIBarButtonItem appearance]
     setBackgroundImage :[UIImage new]
     forState           :UIControlStateNormal
     barMetrics         :UIBarMetricsDefault];

    [[UIBarButtonItem appearance] setTitleTextAttributes:
     (@{ UITextAttributeTextColor: [UIColor LTealColor],
         UITextAttributeFont     : [[UIFont LDefaultFont] fontWithSize:0.] })
                                                forState:UIControlStateNormal];

    [[UINavigationBar appearance] setTitleTextAttributes:
     (@{ UITextAttributeTextShadowColor : [UIColor clearColor],
         UITextAttributeTextColor       : [UIColor LTealColor],
         UITextAttributeFont            : [[UIFont LDefaultFont] fontWithSize:0.] })];
}

- (void)loadNavigationBar
{
    [self.navigationBar setTranslucent:NO];

    [self setNavigationBarHidden:NO];
}

- (void)loadToolbar
{
    [self.toolbar setTranslucent:NO];

    [self setToolbarHidden:NO];
}

@end
