//
//  LUtilityCell.h
//  Writeability
//
//  Created by Ryan on 7/3/14.
//  Copyright (c) 2014 CNL. All rights reserved.
//


#import <UIKit/UIKit.h>


@interface LUtilityCell : UICollectionViewCell

@property (nonatomic, readwrite , strong) NSString  *labelText;
@property (nonatomic, readwrite , strong) NSString  *detailText;

@property (nonatomic, readwrite , strong) NSArray   *buttons;


+ (CGSize)size;

@end
