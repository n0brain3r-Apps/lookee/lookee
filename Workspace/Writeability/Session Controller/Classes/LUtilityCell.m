//
//  LUtilityCell.m
//  Writeability
//
//  Created by Ryan on 7/3/14.
//  Copyright (c) 2014 CNL. All rights reserved.
//


#import "LUtilityCell.h"

#import "LConstants.h"

#import "LToolbar.h"




//*********************************************************************************************************************//
#pragma mark -
#pragma mark LUtilityCell
#pragma mark -
//*********************************************************************************************************************//





@interface LUtilityCell ()

@property (nonatomic, readonly, weak) UILabel   *label;
@property (nonatomic, readonly, weak) UILabel   *detail;

@property (nonatomic, readonly, weak) LToolbar  *toolbar;

@end

@implementation LUtilityCell

#pragma mark - Class Methods
//*********************************************************************************************************************//

+ (CGSize)size
{
    return ((CGSize){48., 48.});
}


#pragma mark - Constructors/Destructors
//*********************************************************************************************************************//

- (instancetype)initWithFrame:(CGRect)frame
{
    if ((self = [super initWithFrame:frame])) {
        UILabel     *label      = [UILabel new];
        UILabel     *detail     = [UILabel new];

        LToolbar    *toolbar    = [LToolbar new];
        [toolbar setBackgroundColor:[UIColor whiteColor]];

        [label setBackgroundColor:[UIColor whiteColor]];
        [label setTextColor:[UIColor LBlackColor]];
        [label setFont:[[UIFont LDefaultFont] fontWithSize:20.]];
        [label setTextAlignment:NSTextAlignmentLeft];

        [detail setBackgroundColor:[UIColor whiteColor]];
        [detail setTextColor:[UIColor LGrayColor]];
        [detail setFont:[label.font fontWithSize:[label.font pointSize]-6.]];
        [detail setTextAlignment:NSTextAlignmentCenter];

        [self.contentView addSubview:detail];
        [self.contentView addSubview:label];
        [self.contentView addSubview:toolbar];

        _label      = label;
        _detail     = detail;
        _toolbar    = toolbar;
    }
    
    return self;
}


#pragma mark - Overridden Methods
//*********************************************************************************************************************//

- (void)layoutSubviews
{
    [super layoutSubviews];

    CGRect frame        = [self bounds];

    CGRect labelFrame   = [self.label frame];
    CGRect detailFrame  = [self.detail frame];
    CGRect toolbarFrame = [self.toolbar frame];

    labelFrame  = ((CGRect) {
        .origin = {CGRectGetMinX(frame), CGRectGetMaxY(frame) - CGRectGetHeight(labelFrame)},
        .size   = {CGRectGetWidth(labelFrame), CGRectGetHeight(labelFrame)}
    });

    detailFrame = ((CGRect) {
        .origin = {CGRectGetMinX(frame), CGRectGetMinY(frame)},
        .size   = {CGRectGetWidth(detailFrame), CGRectGetHeight(detailFrame)}
    });

    toolbarFrame = ((CGRect) {
        .origin = {CGRectGetMaxX(labelFrame), CGRectGetMaxY(frame) - CGRectGetHeight(labelFrame)},
        .size   = {CGRectGetWidth(frame) - CGRectGetWidth(labelFrame), CGRectGetHeight(labelFrame)}
    });

    [self.label setFrame:labelFrame];
    [self.detail setFrame:detailFrame];
    [self.toolbar setFrame:toolbarFrame];
}


#pragma mark - Properties
//*********************************************************************************************************************//

@dynamic labelText;

- (NSString *)labelText
{
    return [self.label text];
}
// TODO: Fix for long labels
- (void)setLabelText:(NSString *)label
{
    [self.label setText:label];
    [self.label sizeToFit];

    [self setNeedsLayout];
    [self layoutIfNeeded];
}

@dynamic detailText;

- (void)setDetailText:(NSString *)detail
{
    [self.detail setText:detail];
    [self.detail sizeToFit];

    [self setNeedsLayout];
    [self layoutIfNeeded];
}

@dynamic buttons;

- (NSArray *)buttons
{
    return [self.toolbar rightButtons];
}

- (void)setButtons:(NSArray *)buttons
{
    for (UIView *button in buttons) {
        CGRect frame = [button frame];

        frame.size = [self buttonSize];

        [button setFrame:frame];
    }

    [self.toolbar setRightButtons:buttons];

    [self setNeedsLayout];
    [self layoutIfNeeded];
}


#pragma mark - Private Methods
//*********************************************************************************************************************//

- (CGSize)buttonSize
{
    CGSize  size        = [self toolbar].frame.size;

    CGFloat dimension   = fminf(size.width, size.height);

    return ((CGSize) {
        .width  = dimension,
        .height = dimension
    });
}

@end
