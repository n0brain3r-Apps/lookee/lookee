//
//  LFileHeaderView.m
//  Writeability
//
//  Created by Ryan on 6/4/14.
//  Copyright (c) 2014 CNL. All rights reserved.
//


#import "LFileHeaderView.h"

#import "LConstants.h"

#import <Utilities/LMacros.h>

#import <Utilities/NSObject+Utilities.h>




//*********************************************************************************************************************//
#pragma mark -
#pragma mark LFileHeaderView
#pragma mark -
//*********************************************************************************************************************//




@implementation LFileHeaderView

#pragma mark - Class Methods
//*********************************************************************************************************************//

+ (CGSize)size
{
    return ((CGSize){48., 48.});
}


#pragma mark - Constructors/Destructors
//*********************************************************************************************************************//

- (instancetype)initWithFrame:(CGRect)frame
{
    if ((self = [super initWithFrame:frame])) {
        [self setBackgroundColor:[UIColor whiteColor]];

        UILabel *label  = [[UILabel alloc] initWithFrame:frame];

        [label setBackgroundColor:[UIColor clearColor]];
        [label setTextColor:[UIColor LTealColor]];
        [label setTextAlignment:NSTextAlignmentCenter];
        [label setFont:[[UIFont LDefaultFont] fontWithSize:18.]];

        [label setAutoresizingMask:(UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight)];

        UIImageView *button = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"back"]];

        [button setAutoresizingMask:UIViewAutoresizingFlexibleRightMargin];
        [button setUserInteractionEnabled:YES];

        [button setCenter:((CGPoint) {
            .x = CGRectGetMinX(self.bounds) + CGRectGetWidth(button.frame),
            .y = CGRectGetMidY(self.bounds)
        })];

        UITapGestureRecognizer *recognizer = [UITapGestureRecognizer new];

        [recognizer addTarget:self action:@selector(tapGestureRecognized:)],
        [self.class addMethodWithSelector:@selector(tapGestureRecognized:) withBlockImplementation:

         ^(typeof(self) self, UITapGestureRecognizer *recognizer) {
             if ([self dismissBlock]) {
                 [self dismissBlock]();
             }
         }

         ];

        [button addGestureRecognizer:recognizer];

        [self addSubview:button];
        [self addSubview:label];

        _titleLabel = label;
    }

    return self;
}

@end
